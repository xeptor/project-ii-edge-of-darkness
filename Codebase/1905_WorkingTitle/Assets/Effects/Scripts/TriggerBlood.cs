﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBlood : MonoBehaviour
{
    public AudioSource[] audioSources;
    public GameObject player;
    public GameObject hud;
    PlayerElements plElements;
    ParticleSystem bloodEffect;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        hud = player.transform.Find("HUD").gameObject;
        bloodEffect = GetComponent<ParticleSystem>();
        plElements = hud.GetComponent<PlayerElements>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        bloodEffect = player.transform.Find("BloodEffectParent").transform.Find("BloodEffect").GetComponent<ParticleSystem>();
    }
    void Update()
    {
        if (plElements.playerTookDamage)
        {
            bloodEffect.Play();
            playSound();
        }
    }
    void playSound()
    {
        int pickSound = Random.Range(0, audioSources.Length);
        audioSources[pickSound].Play();
    }
}
