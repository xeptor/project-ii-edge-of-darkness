﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCharge : StateMachineBehaviour
{
    private Transform playerPos;
    private GameObject dragon;
    private BossController controller;
    public int speed = 20;
    public AudioClip run;
    public GameObject sound;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        dragon = GameObject.FindGameObjectWithTag("Boss");
        controller = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossController>();
        controller.knownLocation = GameObject.FindGameObjectWithTag("Player").transform.position;
        sound.GetComponent<audioManager>().PlayEnemySound(run);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        if (Vector3.Distance(controller.knownLocation, dragon.transform.position) > 1.5f)
        {
            animator.transform.position = Vector3.MoveTowards(dragon.transform.position, controller.knownLocation, speed * Time.deltaTime);
            
        }
        else 
        {
            animator.SetBool("WingAttack", true);
        }
       
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
