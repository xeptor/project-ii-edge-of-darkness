﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossWalkFollow : StateMachineBehaviour
{
    private Transform playerPos;
    private GameObject dragon;
    private BossController controller;
    public int speed = 10;
    public AudioClip walk;
    public GameObject sound;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        dragon = GameObject.FindGameObjectWithTag("Boss");
        controller = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossController>();
        sound.GetComponent<audioManager>().PlayEnemySound(walk);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RotateTowards(playerPos);
        if (controller.health <= 600)
        {
            animator.SetTrigger("ChargeAttack");
            animator.SetBool("isFollowing", false);
        }

        else 
        {
            if (controller.FollowCheck())
                animator.transform.position = Vector3.MoveTowards(dragon.transform.position, playerPos.position, controller.speed * Time.deltaTime);
            if (controller.AttackCheck())
            {
                if (controller.health > 600)
                {
                    animator.SetBool("BiteAttack", true);
                    animator.SetBool("isFollowing", false);
                }


            }
        }
        
       
    }
    bool AttackCheck()
    {
        var playerDistance = Vector3.Distance(playerPos.transform.position, dragon.transform.position);

        if (playerDistance <= controller.attackDistance)
        {
            return true;
        }
        return false;
    }
    void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - dragon.transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        dragon.transform.rotation = Quaternion.Slerp(dragon.transform.rotation, lookRotation, Time.deltaTime * 10f);
    }
    //void SelectPath()
    //{
    //    NavMeshPath path = new NavMeshPath();

    //    int randomDir = Random.Range(0, 10);
    //    if (randomDir < 3) //left
    //    {
    //        Vector3 offset = transform.position - player.transform.position;
    //        moveDirection = Vector3.Cross(offset, Vector3.up);
    //        navMeshAgent.CalculatePath(player.transform.position + moveDirection, path);
    //        navMeshAgent.SetPath(path);
    //    }
    //    else if (randomDir >= 3 && randomDir < 5) //right
    //    {
    //        Vector3 offset = player.transform.position - transform.position;
    //        moveDirection = Vector3.Cross(offset, Vector3.up);
    //        navMeshAgent.CalculatePath(player.transform.position + moveDirection, path);
    //        navMeshAgent.SetPath(path);
    //    }
    //    else
    //    {
    //        navMeshAgent.CalculatePath(player.transform.position, path);
    //        navMeshAgent.SetPath(path);
    //    }
    //}
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
