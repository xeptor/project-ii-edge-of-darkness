﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDeathCheck : StateMachineBehaviour
{

    private BossController controller;
    private GameObject dragon;
    private Transform playerPos;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossController>();
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        dragon = GameObject.FindGameObjectWithTag("Boss");
        controller.isVulnerable = false;

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RotateTowards(playerPos);
        if (controller.health <= 0)
        {
            animator.SetBool("isDead", true);
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject.FindGameObjectWithTag("Boss").GetComponentInChildren<SkinnedMeshRenderer>().material.SetTexture("_MainTex", controller.normal);
        
    }

    void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - dragon.transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        dragon.transform.rotation = Quaternion.Slerp(dragon.transform.rotation, lookRotation, Time.deltaTime * 10f);
    }
    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
