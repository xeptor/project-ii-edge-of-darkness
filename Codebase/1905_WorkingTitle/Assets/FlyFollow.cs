﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyFollow : StateMachineBehaviour
{
    private Transform playerPos;
    private GameObject dragon;
    private BossController controller;
    public AudioClip wingFlaps;
    public int speed = 10;
    public GameObject sound;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        dragon = GameObject.FindGameObjectWithTag("Boss");
        controller = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossController>();
        sound.GetComponent<audioManager>().PlayEnemySound(wingFlaps);
        
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller.RotateTowards(playerPos);
        if (controller.FollowCheck())
        {
            animator.transform.position = Vector3.MoveTowards(dragon.transform.position, playerPos.position, controller.speed * Time.deltaTime);
        }

        if (controller.FlyAttackCheck())
        {
            animator.SetBool("FlyFlame", true);
            animator.SetBool("isFollowing", false);


        }
    }
    
   
}
