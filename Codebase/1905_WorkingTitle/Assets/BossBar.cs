﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BossBar : MonoBehaviour
{
    public GameObject dragon;
    public Image healthBar;
    public float min;
    public float max = 800;
    private float currHealth;
    private float currPercent;

    public bool barActive = false;
    // Start is called before the first frame update
    void Start()
    {
        SetHealth(dragon.GetComponent<BossController>().maxHealth);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (dragon != null && !dragon.GetComponent<BossController>().isDead)
        {
            SetHealth(dragon.GetComponent<BossController>().health);
        }
    }

    public void SetHealth(float health)
    {
        if(health != currHealth)
        {
            currHealth = health;
            currPercent = health / max;
        }

        if (currPercent <= 0 && dragon.GetComponent<BossController>().isDead)
        {
            healthBar.fillAmount = min;
        }
        else
        {
            healthBar.fillAmount = currPercent;
        }
    }
}
