﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CooldownPowerBar : MonoBehaviour
{
    public GameObject player;
    public Image powerBar;
    public float min;
    public float max = 100;
    private float curr;
    private float currPercent;
    // Start is called before the first frame update
    void Start()
    {
        Cooldown(player.GetComponent<PlayerController>().maxPowerCooldown);
    }

    // Update is called once per frame
    void Update()
    {
        Cooldown(player.GetComponent<PlayerController>().currPowerCooldown);
    }

    public void Cooldown(float current)
    {
        if (current != curr)
        {
            curr = current;
            currPercent = current / max + .1f;
        }

        if (currPercent <= 0)
        {
            powerBar.fillAmount = max;
        }
        else
        {
            powerBar.fillAmount = currPercent;
        }
    }
}
