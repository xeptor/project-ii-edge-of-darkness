﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyIdle : StateMachineBehaviour
{
    private GameObject dragon;
    private Transform playerPos;
    private BossController controller;
    public AudioClip wingFlaps;
    public GameObject sound;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        dragon = GameObject.FindGameObjectWithTag("Boss");
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        controller = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossController>();
        sound.GetComponent<audioManager>().PlayEnemySound(wingFlaps);
        dragon.GetComponent<Collider>().enabled = false;
       
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller.RotateTowards(playerPos);
        if (controller.flyingComplete == 3)
        {
            animator.SetBool("Fly", false);
            animator.SetBool("isFollowing", false);
        }
        else if (controller.FlyAwayCheck())
        {
            
            animator.SetBool("FlyAway", true);
            animator.SetBool("isFollowing", false);
        }
        else if (controller.FlyFollowCheck())
        {
            animator.SetBool("isFollowing", true);
        }

        else if (controller.FlyAttackCheck())
        {            
            animator.SetBool("FlyFlame", true);
            animator.SetBool("isFollowing", false);          
        }
    }    
}
