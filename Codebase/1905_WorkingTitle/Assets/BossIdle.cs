﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIdle : StateMachineBehaviour
{
    private GameObject dragon;
    private Transform playerPos;
    private BossController controller;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        dragon = GameObject.FindGameObjectWithTag("Boss");
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        controller = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossController>();
        if (FollowCheck())
            animator.SetBool("isFollowing", true);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RotateTowards(playerPos);
        if (controller.health <= 600)
        {
            animator.SetTrigger("ChargeAttack");
            animator.SetBool("isFollowing", false);
        }

        else 
        {
            if(FollowCheck())
            animator.SetBool("isFollowing", true);

            if (controller.AttackCheck())
            {
                if (controller.health > 600)
                {
                    animator.SetBool("BiteAttack", true);
                    animator.SetBool("isFollowing", false);
                }
            }
        }
    }

    void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - dragon.transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        dragon.transform.rotation = Quaternion.Slerp(dragon.transform.rotation, lookRotation, Time.deltaTime * 10f);
    }
    bool FollowCheck()
    {
        var playerDistance = Vector3.Distance(playerPos.transform.position, dragon.transform.position);

        if (playerDistance > controller.aggroDistance)
        {
            return true;
        }
        return false;
    }
    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
