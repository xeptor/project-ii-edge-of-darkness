﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "MovingPlatform")
        {
            //This will make the player a child of the Obstacle
            Debug.Log("movedParent");
            transform.SetParent(other.gameObject.transform);//Change "myPlayer" to your player
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "MovingPlatform")
        {
            transform.parent = null;
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
