﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossVulnerable : StateMachineBehaviour
{

    private BossController controller;
    public AudioClip defenseGrowl;
    private GameObject dragon;
    private Transform playerPos;
    public GameObject sound;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        controller = GameObject.FindGameObjectWithTag("Boss").GetComponent<BossController>();
        controller.isVulnerable = true;
        sound.GetComponent<audioManager>().PlayEnemySound(defenseGrowl);
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        dragon = GameObject.FindGameObjectWithTag("Boss");
        dragon.GetComponent<BossController>().knownLocation = Vector3.zero;
        animator.SetBool("BiteAttack", false);
        animator.SetBool("WingAttack", false);

    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        RotateTowards(playerPos);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        controller.isVulnerable = false;
        
    }

    void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - dragon.transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        dragon.transform.rotation = Quaternion.Slerp(dragon.transform.rotation, lookRotation, Time.deltaTime * 10f);
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
