﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{
    public Transform player;
    public SlidingWall[] slidingWalls;
    public GameObject[] planes;
    private int i;

    void Update()
    {
        i = 0;
        foreach (SlidingWall sw in slidingWalls)
        {
            if (sw.GetInside())
            {
                planes[i].SetActive(false);
            }
            i++;
        }
    }

    private void LateUpdate()
    {
        Vector3 newPosition = player.position;
        newPosition.y = transform.position.y;
        transform.position = newPosition;
    }
}
