﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDamage : MonoBehaviour
{
    public GameObject player;
    public GameObject hud;
    PlayerElements plElements;
    float playerHealth;
    public float damageAmount = 10;
    public float knockback = 10;
    public bool poisonous = false;
    public float poisonDamage = 5;
    public int poisonTimer = 10;
    
    void Start()
    {
        player = GameObject.Find("Player");
        hud = GameObject.Find("HUD").gameObject;
        plElements = hud.GetComponent<PlayerElements>();
        playerHealth = plElements.currentHealth;
    }
    
    void FixedUpdate()
    {
        playerHealth = plElements.currentHealth;
    }
    

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            player.GetComponent<PlayerController>().TakeDamage(damageAmount, (player.transform.position - transform.position), knockback, poisonous, poisonTimer, poisonDamage);
        }
    }
}
