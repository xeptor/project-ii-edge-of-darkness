﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explode : MonoBehaviour
{

    public GameObject player;
   // float selfDestroyTime = 2f;
    float selfDestroyCount;
    public bool hit = false;
    public ParticleSystem fireball;
    public ParticleSystem explosion;

    public AudioClip flying;
    public AudioClip explode;
    GameObject sound;
    // Start is called before the first frame update
    void Start()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager");

        fireball.Play();
        explosion.Stop();
        //audioManager.instance.PlaySound(flying);
        sound.GetComponent<audioManager>().PlaySound(flying);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        //explode fireball
        //stop movement
        //set playr damage from player
        hit = true;
        
        if (other.gameObject.tag != "Player" && other.gameObject.tag != "PlayerSword")
        {
            transform.GetComponent<SphereCollider>().radius = 3;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            fireball.Stop();
            //audioManager.instance.PlaySound(explode);
            sound.GetComponent<audioManager>().PlaySound(explode);
            explosion.Play();
            
            
            
            
            
        }

       
    }
}
