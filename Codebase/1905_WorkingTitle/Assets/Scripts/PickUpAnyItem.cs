﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpAnyItem : MonoBehaviour
{
    public AudioClip pickUpItem;
    public GameObject hud;
    private GameObject player;
    GameObject sound;
    private bool entered;
    private void Start()
    {
        hud = GameObject.Find("HUD");
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        player = GameObject.FindGameObjectWithTag("Player");
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            entered = true;
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
            entered = false;
    }
    private void Update()
    {
        if (entered)
        {
            //check to see if potions are full
            if (this.gameObject.tag == "HealthPotion" && hud.GetComponent<PlayerElements>().CheckVials())
            {
                // Add one to potion count
                hud.GetComponent<PlayerElements>().GetVial();
                sound.GetComponent<audioManager>().PlaySound(pickUpItem);
                Destroy(gameObject);
            }
            else if (this.gameObject.tag == "Key")
            {
                // update hasKey bool
                player.gameObject.GetComponent<PlayerController>().hasKey = true;
                sound.GetComponent<audioManager>().PlaySound(pickUpItem);
                Destroy(gameObject);
            }
            else if (this.gameObject.tag == "FirePotion" && hud.GetComponent<PlayerElements>().CheckFlasks())
            {
                hud.GetComponent<PlayerElements>().GetFire();
                sound.GetComponent<audioManager>().PlaySound(pickUpItem);
                Destroy(gameObject);
            }
        }
    }
}
