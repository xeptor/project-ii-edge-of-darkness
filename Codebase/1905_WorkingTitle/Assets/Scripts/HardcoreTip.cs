﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HardcoreTip : MonoBehaviour
{
    public GameObject mainMenu;
    private void Start()
    {
        
    }

    void Update()
    {
        GetComponent<UnityEngine.UI.Text>().enabled = mainMenu.GetComponent<MainMenu>().isHardcore;
    }
}
