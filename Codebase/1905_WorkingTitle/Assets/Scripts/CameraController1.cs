﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CameraController1 : MonoBehaviour
{
    Vector2 mouseLook;
    Vector2 smoothV;
    [SerializeField]
    public float xSensitivity = 5.0f;
    [SerializeField]
    public float ySensitivity = 5.0f;
    
   

    
    float mouseY;

    GameObject player;

    private void Awake()
    {
        Cursor.visible = false; //Take out for web
        Cursor.lockState = CursorLockMode.Locked;
    }
    // Start is called before the first frame update
    void Start()
    {
        player = this.transform.parent.gameObject;
        //LoadSensitivity();
        if ((PlayerPrefs.GetFloat("Mouse X") != 0 && PlayerPrefs.GetFloat("Mouse Y") != 0))
        {
            LoadSensitivity();
        }
        else
        {
            xSensitivity = 30;
            ySensitivity = 15;
        }

    }

    // Update is called once per frame
    void Update()
    {

        //Left and Right
        float mouseX = xSensitivity * Input.GetAxis("Mouse X") * Time.deltaTime;
        player.transform.Rotate(0, mouseX, 0, Space.World);

        //Up and Down

        mouseY += ySensitivity * Input.GetAxis("Mouse Y") * Time.deltaTime;
        mouseY = Mathf.Clamp(mouseY, -45, 45);
        Vector3 look = new Vector3(mouseY, 0, 0);
        transform.localEulerAngles = new Vector3(-mouseY, 0, 0);

        //if (Input.GetKeyDown("escape"))
        //{
        //    Cursor.lockState = CursorLockMode.None;
        //}
    }

    public void LoadSensitivity()
    {
        xSensitivity = PlayerPrefs.GetFloat("Mouse X");
        ySensitivity = PlayerPrefs.GetFloat("Mouse Y");
    }
}

