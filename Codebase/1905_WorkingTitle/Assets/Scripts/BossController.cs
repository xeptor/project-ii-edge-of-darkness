﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class BossController : MonoBehaviour
{
    


    public GameObject player;
    PlayerController playerScript;
    public GameObject keyDrop;
    public GameObject fireDrop;
    public NavMeshAgent navMeshAgent;
    ParticleSystem particleEffect;
    Animator animator;   
    Rigidbody body;

    //display boss health
    public bool bossLevel = false;
    public GameObject hud;

    //set texture materials: normal / damaged
    public Texture normal;
    public Texture damaged;
   

    //Spell Casting
    public ParticleSystem flames;
    
    public GameObject fireBreath;
    public GameObject mightyProjectile;
    GameObject tempObj;
    public int flameSpeed = 5;

    public float health;
    public bool isDead = false;
    public int speed = 2;
    public float maxHealth = 800;

    //starting state of the boss until first hit
    public bool isAsleep = true;    

    //bool turning off at times when the boss can be damaged
    public bool isVulnerable = true;

    //the distance away from the player the boss can attack and move towards while on the ground and flying
    public int aggroDistance = 0;
    public int attackDistance = 0;
    public int flyAggroDistance = 0;

    //a count keeping track of how many times the boss performs flying attacks; max 3
    public int flyingComplete = 0;

    //Enenmy direction
    Vector3 moveDirection;

    //Attack count
    public int attackCount = 0;

    //players last known location
    public Vector3 knownLocation;

    //dragon sounds

   
    public AudioClip dragonGrunt;
    public AudioClip invincible;
    GameObject sound;

    public ParticleSystem blood;
    
    
    void Start()
    {
        animator = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        particleEffect = GetComponentInChildren<ParticleSystem>();
        flames.Stop();
        hud.GetComponentInChildren<BossBar>().barActive = true;
        health = maxHealth;
        
    }
   

    void OnTriggerEnter(Collider other)
    {
        //Check for collison with the Players sword
        if (other.gameObject.tag == "PlayerSword" && isVulnerable)
        {
            //Update Health            
            animator.SetTrigger("damage");   

            if(!isAsleep)
            {
                health -= 100;
            }
            sound.GetComponent<audioManager>().PlayEnemySound(dragonGrunt);
            //audioManager.instance.PlayEnemySound(dragonGrunt);
            //save default values of mesh filter/color
            GetComponentInChildren<SkinnedMeshRenderer>().material.SetTexture("_MainTex", damaged);
            blood.Play();
            blood.Play();
            blood.Play();
            blood.Play();

            //reset mesh color to default
            isAsleep = false;            
            isVulnerable = false;
        }
        else if (other.gameObject.tag == "PlayerSword" && !isVulnerable)
        {
            sound.GetComponent<audioManager>().PlaySound(invincible);
            isVulnerable = false;
        }
        
    }
    
    
    //checks if the boss is close enough to the player to attack
    public bool AttackCheck()
    {
        var playerDistance = Vector3.Distance(player.transform.position, transform.position);

        if (playerDistance <= aggroDistance)
        {
            return true;
        }
        return false;
    }
    //checks if the boss is close enough to the player to attack while flying
    public bool FlyAttackCheck()
    {
        var playerDistance = Vector3.Distance(player.transform.position, transform.position);

        if (playerDistance <= flyAggroDistance)
        {
            return true;
        }
        return false;
    }
    //checks if the player is hiding under the boss
    public bool FlyAwayCheck()
    {
        var playerDistance = Vector3.Distance(player.transform.position, transform.position);

        if (playerDistance <= 3)
        {
            return true;
        }
        return false;
    }
    //checks if the boss is far enough from the player to move towards them
    public bool FollowCheck()
    {
        var playerDistance = Vector3.Distance(player.transform.position, transform.position);
        
        if (playerDistance >= aggroDistance)
        {
            return true;
        }
        return false;
    }
    //checks if the boss is far enough from the player to move towards them while flying
    public bool FlyFollowCheck()
    {
        var playerDistance = Vector3.Distance(player.transform.position, transform.position);

        if (playerDistance >= flyAggroDistance)
        {
            return true;
        }
        return false;
    }   
    //called when boss is defeated
    public void Death()
    {
        LootDrop();
        isDead = true;
        Destroy(this.gameObject, 5f);
        hud.GetComponentInChildren<BossBar>().barActive = false;
    }
    //faces the boss in the direction of the player
    public void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10f);
    }
    //dragon flame breath
    public void FlameBreathAttack()
    {
        Transform origin = GameObject.Find("FlameTongue").transform;
        tempObj = Instantiate(fireBreath, origin.position, player.transform.rotation) as GameObject;
        
        
        Destroy(tempObj, .5f);
    }
    //drops any loot the boss is holding when defeated
    void LootDrop()
    {
        if (GetComponent<Loot>().hasKey == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(keyDrop, newPos, transform.rotation);
        }
        if (GetComponent<Loot>().hasFire == true && GetComponent<Loot>().hasKey == false)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(fireDrop, newPos, transform.rotation);
        }
        else if (GetComponent<Loot>().hasFire == true && GetComponent<Loot>().hasKey == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            newPos.x += 1;
            Instantiate(fireDrop, newPos, transform.rotation);
        }
    }
}
