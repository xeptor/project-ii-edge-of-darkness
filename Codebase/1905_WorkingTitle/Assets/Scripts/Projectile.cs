﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    public GameObject bullet;
    public float bulletSpeed = 0;
    float selfDestroyTime = 2f;
    float selfDestroyCount;
    //float blastRadius = 2f;
    public GameObject player;
    SphereCollider aoe;
    GameObject tempObj;
    public float fireDamage = 200;
    //public ParticleSystem fireball;
    //public ParticleSystem explosion;

    private void Update()
    {
        //selfDestroyCount -= Time.deltaTime;

        //if (selfDestroyCount <= 0)
        //{
        //    Destroy(tempObj);
        //}
        //tempObj.transform.position += player.transform.forward * bulletSpeed * Time.deltaTime;
       
    }

    public void ShootProjectile()
    {

        //Instantiate/Create Bullet
        
        selfDestroyCount = selfDestroyTime;
        tempObj = Instantiate(bullet) as GameObject;
        player.GetComponent<PlayerController>().damageToEnemy = fireDamage;
        //fireball.Play();
        //Set position  of the bullet in front of the player
        tempObj.transform.position = transform.position + player.transform.forward;
        tempObj.GetComponent<Rigidbody>().AddForce(transform.forward * bulletSpeed, ForceMode.Impulse);
        Destroy(tempObj, 1.46f);
        
        
    }
   
    
}
