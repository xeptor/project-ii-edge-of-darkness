﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    //normal attack
    public float damage;
    public float knockback;
    public bool poisonous;
    public int poisonTimer;
    public float poisonDamage;
    
    //special attack
    public float specialDamage;
    public float specialKnockback;
    public bool specialPoisonous;
    public int specialPoisonTimer;
    public float specialPoisonDamage;


    void Start()
    {
        
    }
    void Update()
    {
        
    }
}
