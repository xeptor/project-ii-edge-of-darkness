﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlmostDedScreen : MonoBehaviour
{
    public PlayerElements playerStats;
    public Image overlay;
    // Start is called before the first frame update
    void Start()
    {
        playerStats = GameObject.Find("HUD").GetComponent<PlayerElements>();
        overlay = transform.Find("overlay").GetComponent<Image>();
        overlay.CrossFadeAlpha(0, 0, false);
    }

    // Update is called once per frame
    void Update()
    {
        if (playerStats.currentHealth < (playerStats.maxHealth * .25))
        {
            overlay.CrossFadeAlpha(1, 1, false);
        }
        else
        {
            overlay.CrossFadeAlpha(0, 1, false);
        }
    }
}
