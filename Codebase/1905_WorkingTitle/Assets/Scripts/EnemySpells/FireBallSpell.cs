﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallSpell : MonoBehaviour
{
    public GameObject projectile;
    GameObject tempObj;

    public void ShootProjectile()
    {
        tempObj = Instantiate(projectile) as GameObject;
        tempObj.transform.position += Vector3.forward;

        Rigidbody rb = tempObj.GetComponent<Rigidbody>();
        rb.velocity = transform.forward * 20;
        Destroy(tempObj, 5f);
    }
}
