﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpellDamage : MonoBehaviour
{
    public GameObject player;
    public GameObject hud;
    PlayerElements plElements;
    public float damageAmount = 10;
    public float knockback = 10;
    public bool poisonous = false;
    public float poisonDamage = 5;
    public int poisonTimer = 10;

    //Indicator
    public RectTransform indicatorRect;
    public Image indicatorImage;
    float angle = 0;
    Color targetAlpha;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        hud = GameObject.Find("HUD").gameObject;
        plElements = hud.GetComponent<PlayerElements>();

        indicatorImage = GameObject.FindGameObjectWithTag("indicatorImage").transform.Find("indicator").GetComponent<Image>();
        indicatorRect = indicatorImage.GetComponent<RectTransform>();
        indicatorImage.CrossFadeAlpha(0, 0, false);
    }

    void OnTriggerEnter(Collider other)
    {
        //explode fireball
        //stop movement

        if (other.gameObject.tag == "Player")
        {
            if (gameObject.tag == "Projectile")
            {
                GetComponent<Rigidbody>().velocity = Vector3.zero;
                transform.GetComponent<SphereCollider>().radius = 3;
                //Destroy spell
                Destroy(this.gameObject);
            }
            player.GetComponent<PlayerController>().TakeDamage(damageAmount, (player.transform.position - transform.position), knockback, poisonous, poisonTimer, poisonDamage);

            // Angle between other pos vs player
            angle = GetHitAngle(other.transform, transform.forward);
            indicatorImage.CrossFadeAlpha(1, 0, false);
            indicatorRect.rotation = Quaternion.Euler(0, 0, -angle);
            indicatorImage.CrossFadeAlpha(0, 1, false);


        }
    }

    public float GetHitAngle(Transform player, Vector3 incomingDirection)
    {
        // Flatten to plane
        var otherDir = new Vector3(-incomingDirection.x, 0f, -incomingDirection.z);
        var playerFwd = Vector3.ProjectOnPlane(player.forward, Vector3.up);

        // Direction between player fwd and incoming object
        var angle = Vector3.SignedAngle(playerFwd, otherDir, Vector3.up);

        return angle;
    }
}
