﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    GameObject loader;
    LevelLoader levelLoader;
    AudioSource menuAudio;
    GameObject sound;
    public Toggle hardcore;

    public bool isHardcore = false;

    public void Start()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        if (PlayerPrefs.GetInt("Hardcore") != 0)
        {
            isHardcore = false;
            hardcore.isOn = true;
        }
        loader = GameObject.Find("LevelLoader");
        levelLoader = loader.GetComponent<LevelLoader>();
    }

    public void PlayGame()
    {
        if (isHardcore)
            PlayerPrefs.SetInt("Hardcore", 1);
        else
            PlayerPrefs.SetInt("Hardcore", 0);

        levelLoader.FadeToLevel(1);
    }

    public void ToggleHardcore()
    {
        if (!isHardcore)
            isHardcore = true;
        else if (isHardcore)
            isHardcore = false;
    }

    public void QuitGame()
    {
        Debug.Log("Exit");
        Application.Quit();
    }
}
