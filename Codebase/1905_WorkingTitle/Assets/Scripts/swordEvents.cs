﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swordEvents : MonoBehaviour
{

    Animator swordEvent;
    public CapsuleCollider sword;
    public CapsuleCollider fangs;
    public CapsuleCollider flames;
    public GameObject eyes;
    GameObject tempEffect;
    public GameObject player;
    GameObject sound;

    // Start is called before the first frame update
    void Start()
    {
        swordEvent = GetComponent<Animator>();
        player = GameObject.Find("Player");
        sound = GameObject.FindGameObjectWithTag("SoundManager");
    }

    void TurnOnCollider()
    {
        sword.enabled = true;
    }
    void TurnOffCollider()
    {
        sword.enabled = false;
    }
    void TurnOnFlameCollider()
    {
        flames.enabled = true;
    }
    void TurnOffFlameCollider()
    {
        flames.enabled = false;
    }
    void TurnOnFangs()
    {
        fangs.enabled = true;
    }
    void TurnOffFangs()
    {
        fangs.enabled = false;
    }


    void TurnOnEmissive()
    {
        eyes.gameObject.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
    }
    void TurnOffEmissive()
    {
        eyes.gameObject.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
    }

    void PlayAudio(AudioClip sfx)
    {
        //audioManager.instance.PlaySound(sfx);
        sound.GetComponent<audioManager>().PlaySound(sfx);
    }
    void PlayEnemyAudio(AudioClip sfx)
    {
        //audioManager.instance.PlayEnemySound(sfx);
        sound.GetComponent<audioManager>().PlayEnemySound(sfx);
    }

    void ParticleEffect(GameObject effect)
    {
        tempEffect = Instantiate(effect, transform.position + Vector3.up, effect.transform.rotation);
        Destroy(tempEffect, 1.5f);
    }

    void PoisonPlayer()
    {
        player.GetComponent<PlayerController>().isPoisoned = true;
    }
    void CurePlayer()
    {
        player.GetComponent<PlayerController>().isPoisoned = false;
    }
    //For Demon
    void TurnOnEmissiveDemon()
    {
        Material[] mats = eyes.gameObject.GetComponent<Renderer>().materials;
        //mats[3].color = Color.red;
        mats[3].EnableKeyword("_EMISSION");
        //eyes.gameObject.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
    }
    void TurnOffEmissiveDemon()
    {
        Material[] mats = eyes.gameObject.GetComponent<Renderer>().materials;
        mats[3].DisableKeyword("_EMISSION");

       // eyes.gameObject.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
    }
}
