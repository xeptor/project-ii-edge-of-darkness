﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using System.IO;

public class OptionsMenu : MonoBehaviour
{
    public AudioMixer mixer;
    public AudioClip[] sfx;
    public Slider musicSlider;
    public Slider sfxSlider;
    private float x;
    private float y;
    private float musicVolume;
    private float sfxVolume;
    GameObject sound;

    private void Start()
    {
        x = PlayerPrefs.GetFloat("Mouse X");
        y = PlayerPrefs.GetFloat("Mouse Y");
        musicVolume = PlayerPrefs.GetFloat("musicVolume");
        sfxVolume = PlayerPrefs.GetFloat("sfxVolume");
            //SetCameraSensitivityX(PlayerPrefs.GetFloat("Mouse X"));
            //SetCameraSensitivityY(PlayerPrefs.GetFloat("Mouse Y"));
            //SetMusicVolume(PlayerPrefs.GetFloat("musicVolume"));
            //SetSFXVolume(PlayerPrefs.GetFloat("sfxVolume"));
            UpdateSliders();
        
        
        sound = GameObject.FindGameObjectWithTag("SoundManager");
    }
    public void SetMusicVolume(float volumeSetting)
    {
        musicVolume = volumeSetting;
        mixer.SetFloat("musicVolume", Mathf.Log10(musicVolume) * 20);
        PlayerPrefs.SetFloat("musicVolume", musicVolume);
    }

    public void SetSFXVolume(float volumeSetting)
    {
        sfxVolume = volumeSetting;        
        //sound.GetComponent<audioManager>().soundEffects.volume = sfxVolume;
        //sound.GetComponent<audioManager>().enemySoundEffect.volume = sfxVolume;
        //sound.GetComponent<audioManager>().walking.volume = sfxVolume;
        //sound.GetComponent<audioManager>().monsterGroans.volume = sfxVolume;
        mixer.SetFloat("sfxVolume", Mathf.Log10(sfxVolume) * 20);
        PlayerPrefs.SetFloat("sfxVolume", sfxVolume);
    }

    public void SetCameraSensitivityX(float xSetting)
    {
        x = xSetting;
        PlayerPrefs.SetFloat("Mouse X", x);
    }

    public void SetCameraSensitivityY(float ySetting)
    {
        y = ySetting;
        PlayerPrefs.SetFloat("Mouse Y", y);
    }

    public void PreviewSoundEffects()
    {
        sound.GetComponent<audioManager>().RandomSound(sfx);
    }

    public void UpdateSliders()
    {
        GameObject.Find("CameraSensitivityX").GetComponent<Slider>().value = PlayerPrefs.GetFloat("Mouse X");
        GameObject.Find("CameraSensitivityY").GetComponent<Slider>().value = PlayerPrefs.GetFloat("Mouse Y");
        GameObject.Find("MusicVolume").GetComponent<Slider>().value = PlayerPrefs.GetFloat("musicVolume");
        GameObject.Find("SFXVolume").GetComponent<Slider>().value = PlayerPrefs.GetFloat("sfxVolume");
    }

    public void DefaultValues()
    {
        PlayerPrefs.SetFloat("Mouse X", 30);
        PlayerPrefs.SetFloat("Mouse Y", 15);
        mixer.SetFloat("musicVolume", 0.227f);
        mixer.SetFloat("sfxVolume", 0.242f);
        PlayerPrefs.SetFloat("musicVolume", 0.227f);
        PlayerPrefs.SetFloat("sfxVolume", 0.242f);

        mixer.GetFloat("musicVolume", out musicVolume);
        mixer.GetFloat("sfxVolume", out sfxVolume);

        UpdateSliders();
    }
}
