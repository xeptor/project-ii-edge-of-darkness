﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class audioManager : MonoBehaviour
{
    public AudioSource soundEffects;
    public AudioSource enemySoundEffect;
    public AudioSource music;
    public AudioSource walking;
    public AudioSource monsterGroans;

    public static audioManager instance = null;

    //private void Awake()
    //{
    //    if (instance == null)
    //    {
    //        instance = this;
    //    }
    //    else if (instance != this)
    //    {
    //        Destroy(this.gameObject);
    //    }

    //    DontDestroyOnLoad(gameObject);
    //}


    public void PlaySound(AudioClip sfx)
    {
        soundEffects.clip = sfx;
        soundEffects.Play();
    }

    public void PlayEnemySound(AudioClip sfx)
    {
        enemySoundEffect.clip = sfx;
        enemySoundEffect.Play();
    }
    public void PlayBackSound(AudioClip sfx)
    {
        music.clip = sfx;
        music.Play();
    }

    public void PlayWalking(AudioClip sfx)
    {
        walking.clip = sfx;
        walking.Play();
    }

    public void PlayGroans(AudioClip sfx)
    {
        monsterGroans.clip = sfx;
        monsterGroans.Play();
    }

    public void RandomSound(params AudioClip[] clips)
    {
        int randomIndex = Random.Range(0, clips.Length);

        soundEffects.clip = clips[randomIndex];

        soundEffects.Play();
    }
}
