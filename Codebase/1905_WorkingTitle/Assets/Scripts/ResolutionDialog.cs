﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResolutionDialog : MonoBehaviour
{
    public int screenWidth;
    public int screenHeight;
    public Dropdown myDropdown;

    void Update()
    {
        switch (myDropdown.value)
        {
            case 0:
                SetScreenWidth(640);
                SetScreenHeight(480);
                break;
            case 1:
                SetScreenWidth(1280);
                SetScreenHeight(720);
                break;
            case 2:
                SetScreenWidth(1920);
                SetScreenHeight(1080);
                break;
            case 3:
                SetScreenWidth(2560);
                SetScreenHeight(1440);
                break;
        }
    }

    public void SetScreenWidth(int width)
    {
        screenWidth = width;
    }

    public void SetScreenHeight(int height)
    {
        screenHeight = height;
    }

    public void SetResolution()//)int width, int height)
    {
        Screen.SetResolution(screenWidth, screenHeight, true);
        Debug.Log("Width" + screenWidth);
        Debug.Log("Height" + screenHeight);
    }
}
