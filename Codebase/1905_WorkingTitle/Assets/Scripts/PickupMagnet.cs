﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupMagnet : MonoBehaviour
{
    public bool isPotion;
    public bool isFlask;
    public GameObject parent;

    private bool entered;
    private float speed;
    private Transform playerPosition;
    private Transform parentPosition;
    private GameObject HUD;
    void Start()
    {
        playerPosition = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        parentPosition = parent.GetComponent<Transform>();
        HUD = GameObject.FindGameObjectWithTag("HUD");
    }
    void FixedUpdate()
    {
        //checks to see if the player has room in their inventory
        if (entered && ((HUD.GetComponent<PlayerElements>().CheckVials() && isPotion) || (HUD.GetComponent<PlayerElements>().CheckFlasks() && isFlask)))
        {
            //makes the object move faster as it gets closer
            speed = 0.5f / ((parentPosition.position - playerPosition.position).magnitude);
            //lerps the parent object
            parentPosition.position = Vector3.MoveTowards(parentPosition.position, playerPosition.position, speed);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
            entered = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
            entered = false;
    }
}
