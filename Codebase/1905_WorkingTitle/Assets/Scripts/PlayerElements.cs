﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerElements : MonoBehaviour
{
    public float maxHealth = 100f;
    public float maxStam = 100f;
    public bool key = false;
    private int maxOrbsOfLight = 3;
    private int maxFirePotion = 3;
    
    
    [HideInInspector]
    public float currentHealth;
    [HideInInspector]
    public float currentStam;

    //regen info
    public float healthRegenRate = 0.002f;
    public float staminaRegenRate = 0.05f;
    

    public Slider healthBar;
    public Slider stamBar;
    public Slider powerAttackCooldown;
    public Image keyImg;

    //potion info
    public int currentFirePotion = 1; // change this to zero once working
    public Image FlaskImg1;
    public Image FlaskImg2;
    public Image FlaskImg3;
    public int currentOrbsOfLight = 0;
    public Image orbImg;
    public Image orbImg2;
    public Image orbImg3;
    public float potionHealValue = 50;
    public float potionStaminaValue = 100;
    private ParticleSystem potionParticles;

    private GameObject player;

    //Sound
    public AudioClip usePotion;
    GameObject sound;

    //in charge of power cooldown



    public bool playerTookDamage { get; set; }
    void Start()
    {
        currentHealth = maxHealth;
        currentStam = maxStam;
        
        potionParticles = GameObject.FindGameObjectWithTag("PotionParticleEffect").GetComponent<ParticleSystem>();
        potionParticles.Stop();
        player = GameObject.FindGameObjectWithTag("Player");
        playerTookDamage = false;

        sound = GameObject.FindGameObjectWithTag("SoundManager");

    }
    private void FixedUpdate()
    {
        if (currentStam < maxStam)
            currentStam += staminaRegenRate;
        if (currentHealth < maxHealth)
            currentHealth += healthRegenRate;

    }
    void Update()
    {
        healthBar.value = CalculateHealth();
        stamBar.value = CalculateStam();
        powerAttackCooldown.value = CalculatePowerAttackCooldownIcon();
        key = player.GetComponent<PlayerController>().hasKey;
        keyImg.enabled = key;
        ShowVials();
        ShowFirePots();
    }

    public float CalculateHealth()
    {
        return currentHealth / maxHealth;
    }
    public float CalculateStam()
    {
        return currentStam / maxStam;
    }
    public float CalculatePowerAttackCooldownIcon()
    {
        return player.GetComponent<PlayerController>().currPowerCooldown / player.GetComponent<PlayerController>().maxPowerCooldown;
    }

    public IEnumerator Damage()
    {
        playerTookDamage = true;
        yield return new WaitForSeconds(.02f);
        playerTookDamage = false;
    }

    void ShowVials()
    {
        //vial 1
        if (currentOrbsOfLight >= 1)
        {
            orbImg.enabled = true;
            orbImg.GetComponentInChildren<Text>().enabled = true;
        }
        else
        {
            orbImg.enabled = false;
            orbImg.GetComponentInChildren<Text>().enabled = false;
        }
        //vial 2
        if (currentOrbsOfLight >= 2)
            orbImg2.enabled = true;
        else
            orbImg2.enabled = false;
        //vial 3
        if (currentOrbsOfLight == 3)
            orbImg3.enabled = true;
        else
            orbImg3.enabled = false;
    }
    void ShowFirePots()
    {
        if (currentFirePotion >= 1)
        {
            FlaskImg1.enabled = true;
            FlaskImg1.GetComponentInChildren<Text>().enabled = true;
        }
        else
        {
            FlaskImg1.enabled = false;
            FlaskImg1.GetComponentInChildren<Text>().enabled = false;
        }
        //vial 2
        if (currentFirePotion >= 2)
            FlaskImg2.enabled = true;
        else
            FlaskImg2.enabled = false;
        //vial 3
        if (currentFirePotion == 3)
            FlaskImg3.enabled = true;
        else
            FlaskImg3.enabled = false;
    }
    public void UsePotion()
    {
        if (currentOrbsOfLight >= 1)
        {
            //removes potion
            currentOrbsOfLight--;
            //increases health
            currentHealth += potionHealValue;
            if (currentHealth > maxHealth)
                currentHealth = maxHealth;
            //increases stamina
                currentStam += potionStaminaValue;
            if (currentStam > maxStam)
                currentStam = maxStam;
            //plays animation
            potionParticles.Play();
            //play potion drink sound
            sound.GetComponent<audioManager>().PlaySound(usePotion);
            //cures player of poison
            player.GetComponent<PlayerController>().Cure();
        }
    }
    public void UseFlask()
    {
        if (currentFirePotion >= 1)
        {
            //removes potion
            currentFirePotion--;
        }
    }
    public void GetVial()
    {
        if (currentOrbsOfLight < maxOrbsOfLight)
            currentOrbsOfLight += 1;
    }
    public void GetFire()   // Cameron - Added so I can finish fire potion
    {
        if (currentFirePotion < maxFirePotion)
            currentFirePotion += 1;
    }
    public bool CheckVials()
    {
        if (currentOrbsOfLight < maxOrbsOfLight)
            return true;
        else return false;
    }
    public bool CheckFlasks()
    {
        if (currentFirePotion < maxFirePotion)
            return true;
        else return false;
    }
}
