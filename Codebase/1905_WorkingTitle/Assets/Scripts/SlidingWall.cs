﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidingWall : MonoBehaviour
{
    public float speed = 0.05f;

    private bool inside = false;
    private Vector3 movementVector = new Vector3(0, -5.1f, 0);
    private Transform wallTransform;

    void Start()
    {
        wallTransform = GetComponent<Transform>();
        movementVector += wallTransform.position;
    }
    void Update()
    {
        //if the player has entered the collider, slide the wall down
        if (inside)
            wallTransform.position = Vector3.MoveTowards(wallTransform.position, movementVector, speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        //when the player enters the collider
        if (other.gameObject.tag == "Player")
        {
            inside = true;
            //make sounds and open the door
            GetComponentInChildren<AudioSource>().Play();
        }
    }

    public bool GetInside()
    {
        return inside;
    }
}
