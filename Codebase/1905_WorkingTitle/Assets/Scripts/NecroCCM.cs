﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NecroCCM : MonoBehaviour
{
    public GameObject player;
    PlayerController playerScript;
    public GameObject itemDrop;
    public GameObject keyDrop;
    public GameObject fireDrop;
    public NavMeshAgent navMeshAgent;
    public GameObject enemyToSpawn;
    ParticleSystem particleEffect;
    Animator animator;
    Collider bodyCollider;

    bool attacking = false;
    bool following = false;
    public bool isDead = false;
    public float attackPause = 3.5f;
    public float attackDistance = 5.5f;
    public float spellAttackDistance = 10f;
    public float meleeAttackDistance = 3f;
    public int aggroDistance = 10;
    public int aggroLossDistance = 25;
    public float health = 350;
    public int enemyFOV = 180;
    float damageFromPlayer;
    int hitCounter = 0;

    List<GameObject> enemies = new List<GameObject>();

    //Enenmy direction
    Vector3 moveDirection;

    //Wander
    public float wanderRadius = 5.0f;

    //Path
    public float maxPathDecisionTime = 0.5f;
    private float pathTimer = 0.0f;

    //Projectile
    public GameObject projectile;
    public GameObject mightyProjectile;
    public GameObject aoeFire;
    public GameObject spawnEffect;
    public AudioClip fireBallSFX;
    public AudioClip fireBall2SFX;
    GameObject tempObj;

    //Sound
    GameObject sound;
    public AudioClip necroHit;
    public AudioClip shieldDeflect;

    //Stages
    public bool isVulnerable = true;
    bool inStage = false;
    int stage = 0;
    void Start()
    {
        //Find the player object
        player = GameObject.Find("Player");
        playerScript = player.GetComponent<PlayerController>();
        animator = GetComponent<Animator>();
        particleEffect = GetComponentInChildren<ParticleSystem>();
        bodyCollider = GetComponent<Collider>();
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        //Enemy begins walking
        WalkWaypoints();

        //Initialize NavMesh path finding timer
        pathTimer = 0.0f;

    }

    void Update()
    {
        float speed = navMeshAgent.velocity.magnitude;
        animator.SetFloat("speed", speed);

        //Increase path decision timer
        pathTimer += Time.deltaTime;

        //Check if health is 0 then execute death sequence
        Death();

        if (!isDead)
        {
            RayCastFollowOrAttack();
        }
    }

    //Random pos
    public static Vector3 RandomNavSphere(Vector3 origin, float distance, int layer)
    {
        Vector3 randomDirection = Random.insideUnitSphere * distance;

        randomDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randomDirection, out navHit, distance, layer);

        return navHit.position;
    }

    void OnTriggerEnter(Collider other)
    {
        //Check for collison with the Players sword
        if (other.gameObject.tag == "PlayerSword")
        {
            //Update Health
            if (health > 0 && !isDead && isVulnerable)
            { 
                Damage();
            }
            if (health > 0 && !isDead && !isVulnerable)
            {
                sound.GetComponent<audioManager>().PlayEnemySound(shieldDeflect);
            }
        }
    }

    private void Damage()
    {
        damageFromPlayer = playerScript.damageToEnemy;
        if (hitCounter == 3)
        {
            animator.SetTrigger("damage");
            hitCounter = 0;
        }
        else
        {
            particleEffect.Play();
            sound.GetComponent<audioManager>().PlayEnemySound(necroHit);
            hitCounter++;
        }
        health -= damageFromPlayer;
    }
    private void RayCastFollowOrAttack()
    {
        //FIND THE PLAYER AND LOCK ON WHEN "SEEN"
        //Distance
        Vector3 playerPos = player.transform.position - transform.position;
        //Angle between player position and forward vector
        float angle = Vector3.Angle(playerPos, transform.forward);

        RaycastHit raycastHit;
        RaycastHit raycastHit2;

        Debug.DrawRay(transform.position, playerPos, Color.green); //For debugging to confirm the ray is working

        //Raycast a distance of 5 units and output the first object the raycast hits
        if (Physics.Raycast(transform.position, playerPos, out raycastHit, aggroDistance))
        {
            //Check if player is in FOV (set in UNITY) and that the returned racyastHit is the player and not a wall
            if (angle < enemyFOV && raycastHit.transform == player.transform)
            {
                //Start following the player
                following = true;
                if (pathTimer > maxPathDecisionTime)
                {
                    pathTimer -= maxPathDecisionTime;
                    SelectPath();
                }
                // Look at the player
                RotateTowards(player.transform);

                if (Vector3.Distance(transform.position, player.transform.position) <= spellAttackDistance)
                {
                    if (Vector3.Distance(transform.position, player.transform.position) <= meleeAttackDistance && !attacking)
                    {
                        StartCoroutine(Attack());
                    }
                    if (!inStage)
                    {
                        animator.SetInteger("stage", 0);
                        StartCoroutine(ChargeToStage());
                    }
                    else if(stage == 1)
                    {
                        animator.SetInteger("stage", 1);
                        animator.SetTrigger("charging");
                    }
                    else if (stage == 2)
                    {
                        animator.SetInteger("stage", 2);
                        animator.SetTrigger("charging");
                    }
                    else if (stage == 3)
                    {
                        animator.SetInteger("stage", 3);
                        animator.SetTrigger("charging");
                    }
                    else if (stage == 4)
                    {
                        animator.SetInteger("stage", 4);
                        animator.SetTrigger("charging");
                    }
                }
            }
        }
        // Check if Player is within a larger "aggroLossDistance" and if the player has been initially aggroed before it starts following
        else if (Physics.Raycast(transform.position, playerPos, out raycastHit2, aggroLossDistance) && following)
        {
            if (pathTimer > maxPathDecisionTime)
            {
                pathTimer -= maxPathDecisionTime;
                SelectPath();
            }
        }
        // If player is farther than aggroLoss, the enemy will no longer follow and walk the waypoints
        else
        {
            following = false;
            WalkWaypoints();
        }
    }

    void WalkWaypoints()
    {
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance) // < stoppingdistance
        {
            Vector3 newPosition = RandomNavSphere(transform.position, wanderRadius, -1);
            navMeshAgent.SetDestination(newPosition);
        }
    }

    void Death()
    {
        if (health <= 0 && !isDead)
        {
            Potion();
            LootDrop();
            isDead = true;
            animator.SetBool("isDead", true);
            bodyCollider.isTrigger = true;
            Destroy(this.gameObject, 6f);
        }
    }

    void RotateTowards(Transform target)
    {
        //yield return new WaitForSeconds(1f);
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10f);
    }

    void Potion()
    {
        Vector3 newPos = transform.position;
        newPos.y += 1;
        Instantiate(itemDrop, newPos, transform.rotation);
    }
    void LootDrop()
    {
        if (GetComponent<Loot>().hasKey == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(keyDrop, newPos, transform.rotation);
        }
        if (GetComponent<Loot>().hasFire == true && GetComponent<Loot>().hasKey == false)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(fireDrop, newPos, transform.rotation);
        }
        else if (GetComponent<Loot>().hasFire == true && GetComponent<Loot>().hasKey == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            newPos.x += 1;
            Instantiate(fireDrop, newPos, transform.rotation);
        }
    }
    void SelectPath()
    {
        NavMeshPath path = new NavMeshPath();

        int randomDir = Random.Range(0, 10);
        if (randomDir < 3) //left
        {
            Vector3 offset = transform.position - player.transform.position;
            moveDirection = Vector3.Cross(offset, Vector3.up);
            navMeshAgent.CalculatePath(player.transform.position + moveDirection, path);
            navMeshAgent.SetPath(path);
        }
        else if (randomDir >= 3 && randomDir < 5) //right
        {
            Vector3 offset = player.transform.position - transform.position;
            moveDirection = Vector3.Cross(offset, Vector3.up);
            navMeshAgent.CalculatePath(player.transform.position + moveDirection, path);
            navMeshAgent.SetPath(path);
        }
        else
        {
            navMeshAgent.CalculatePath(player.transform.position, path);
            navMeshAgent.SetPath(path);
        }
    }

    private float DegreeToRadians(float degree)
    {
        float answer;
        answer = Mathf.PI * (degree / 180.0f);
        return answer;
    }


    //Moved stage stuff to Animator controller

    IEnumerator Attack()
    {
        attacking = true;
        //playerScript.notBeingAttacked = false;
        if (playerScript.notBeingAttacked == false)
        {
            playerScript.notBeingAttacked2 = false;
        }
        else
        {
            playerScript.notBeingAttacked = false;
        }
        animator.SetTrigger("attacking");
        yield return new WaitForSeconds(attackPause);
        attacking = false;
    }

    IEnumerator ChargeToStage()
    {
        inStage = true;
        attacking = true;
        isVulnerable = false;
        animator.SetTrigger("charging");   
        yield return new WaitForSeconds(5f);
        isVulnerable = true;
        attacking = false;
    }

    void SetStage(int _stage)
    {
        stage = _stage;

        if (stage == 4)
        {
            stage = 0;
            inStage = false;
        }
    }
    // Spawns Skeletons (Raise Dead) special attack
    void SpawnEnemyAttack()
    {
        Vector3 newPos = transform.position + Vector3.up;

        for (int i = 0; i < 360; i += 72)//36
        {
            newPos.x += Mathf.Cos(DegreeToRadians(i)) * 3; //multiply by a radius? deree to radian?
            newPos.z += Mathf.Sin(DegreeToRadians(i)) * 3;

            // To-Do Play Sound
            Instantiate(spawnEffect, newPos, transform.rotation);
            tempObj = Instantiate(enemyToSpawn, newPos, transform.rotation) as GameObject;

            newPos = transform.position + Vector3.up;
        }
    }
    // Shoots 10 Fireballs outward in a circle around the enemy    Stage?
    public void ShootProjectilePulse360()
    {
        Vector3 newPos = transform.position + Vector3.up;

        // To-Do Play Sound ?
        for (int i = 0; i < 360; i += 36)//18
        {
            newPos.x += Mathf.Cos(DegreeToRadians(i)) * 3; //multiply by a radius? deree to radian?
            newPos.z += Mathf.Sin(DegreeToRadians(i)) * 3;

            //audioManager.instance.PlayEnemySound(fireBallSFX);
            sound.GetComponent<audioManager>().PlayEnemySound(fireBallSFX);
            tempObj = Instantiate(projectile, newPos, transform.rotation) as GameObject;

            Rigidbody rb = tempObj.GetComponent<Rigidbody>();
            rb.velocity = transform.forward * 7;
            Destroy(tempObj, 3f);

            newPos = transform.position + Vector3.up;

        }
    }
    // Slow moving large fireball     Stage?
    public void ShootMightyProjectile()
    {
        // To-Do Play Sound
        //audioManager.instance.PlayEnemySound(fireBall2SFX);
        sound.GetComponent<audioManager>().PlayEnemySound(fireBall2SFX);
        tempObj = Instantiate(mightyProjectile, transform.position + Vector3.up, transform.rotation) as GameObject;

        Rigidbody rb = tempObj.GetComponent<Rigidbody>();
        rb.velocity = transform.forward * 5;
        Destroy(tempObj, 3f);
    }
    // AOE attack of Wild fire at the players location
    public void aoeWildFire()
    {
        tempObj = Instantiate(aoeFire, player.transform.position, player.transform.rotation) as GameObject;
        Destroy(tempObj, 20f);
    }
}
