﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    private float timer = 0;
    private float durationAlive;
    void Start()
    {
        durationAlive = gameObject.GetComponent<AudioSource>().time;
        gameObject.GetComponent<AudioSource>().Play();
    } 
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= durationAlive)
            Destroy(gameObject, 0);
    }
}
