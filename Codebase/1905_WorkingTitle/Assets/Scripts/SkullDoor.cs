﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullDoor : MonoBehaviour
{
    private ParticleSystem skulls;

    void Start()
    {
        //get skull animation to store in skulls
        skulls = GetComponentInChildren<ParticleSystem>();
        //turn off skull animation intially
        if (skulls != null)
        {
            skulls.Stop();
        }
    }

    void Update()
    {
    }

    void OnTriggerEnter(Collider player)
    {
        //turn on skull animation
        if (player.gameObject.tag == "Player")
        skulls.Play();
    }

    private void OnTriggerExit(Collider player)
    {
        //turn off skull animation
        if (player.gameObject.tag == "Player")
        skulls.Stop();
    }
}