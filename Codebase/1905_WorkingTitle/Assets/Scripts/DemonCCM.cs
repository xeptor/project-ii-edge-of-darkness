﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DemonCCM : MonoBehaviour
{
    public GameObject player;
    PlayerController playerScript;
    public GameObject keyDrop;
    public GameObject fireDrop;
    public NavMeshAgent navMeshAgent;
    ParticleSystem particleEffect;
    Animator animator;
    Collider bodyCollider;
    Rigidbody body;
    bool attacking = false;
    bool following = false;
    public bool isDead = false;
    public float attackPause = 3f;
    public float spellAttackDistance = 10f;
    public float meleeAttackDistance = 3f;
    public float health = 100;
    public int enemyFOV = 180;
    public int aggroDistance = 7;
    public int aggroLossDistance = 30;
    float damageFromPlayer;
    int hitCounter = 0;

    //Enenmy direction
    Vector3 moveDirection;

    //Wander
    public float wanderRadius = 10.0f;

    //Path
    public float maxPathDecisionTime = 0.5f;
    private float pathTimer = 0.0f;

    //Spell Casting
    public GameObject projectile;
    public GameObject mightyProjectile;
    GameObject tempObj;

    //Sound
    GameObject sound;
    public AudioClip demonHit;
    void Start()
    {
        //Find the player object
        player = GameObject.Find("Player");
        playerScript = player.GetComponent<PlayerController>();
        animator = GetComponent<Animator>();
        particleEffect = GetComponentInChildren<ParticleSystem>();
        bodyCollider = GetComponent<Collider>();
        body = GetComponent<Rigidbody>();
        sound = GameObject.FindGameObjectWithTag("SoundManager");

        //Enemy begins walking
        WalkWaypoints();

        //Initialize NavMesh path finding timer
        pathTimer = 0.0f;
    }

    void Update()
    {
        //Increase path decision timer
        pathTimer += Time.deltaTime;
        float speed = navMeshAgent.velocity.magnitude;
        animator.SetFloat("speed", speed);

        //Check if health is 0 then execute death sequence
        Death();

        if (!isDead)
        {
            RayCastFollowOrAttack();
        }
    }

    //Random pos
    public static Vector3 RandomNavSphere(Vector3 origin, float distance, int layer)
    {
        Vector3 randomDirection = Random.insideUnitSphere * distance;

        randomDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randomDirection, out navHit, distance, layer);

        return navHit.position;
    }

    void OnTriggerEnter(Collider other)
    {
        //Check for collison with the Players sword
        if (other.gameObject.tag == "PlayerSword")
        {
            //Update Health
            if (health > 0 && !isDead)
            {
                StartCoroutine(Damage());
            }
        }
    }



    IEnumerator Damage()
    {
        damageFromPlayer = playerScript.damageToEnemy;
        if (hitCounter == 3)
        {
            animator.SetTrigger("damage");
            hitCounter = 0;
        }
        else
        {
            particleEffect.Play();
            sound.GetComponent<audioManager>().PlayEnemySound(demonHit);
            hitCounter++;
        }
        health -= damageFromPlayer;
        if (damageFromPlayer > 199 && health > 0)
        {
            navMeshAgent.enabled = false;
            body.isKinematic = false;
            Vector3 direction = (player.transform.position - transform.position).normalized;
            body.AddForce(direction.x * -750, 0, direction.z * -750);
            yield return new WaitForSeconds(.4f);
            navMeshAgent.enabled = true;
            body.isKinematic = true;
        }
        else if (damageFromPlayer > 50 && health > 0)
        {
            navMeshAgent.enabled = false;
            body.isKinematic = false;
            Vector3 direction = (player.transform.position - transform.position).normalized;
            body.AddForce(direction.x * -500, 0, direction.z * -500);
            yield return new WaitForSeconds(.4f);
            navMeshAgent.enabled = true;
            body.isKinematic = true;
        }
    }

    private void RayCastFollowOrAttack()
    {
        //FIND THE PLAYER AND LOCK ON WHEN "SEEN"
        //Distance
        Vector3 playerPos = player.transform.position - transform.position;
        //Angle between player position and forward vector
        float angle = Vector3.Angle(playerPos, transform.forward);

        RaycastHit raycastHit;
        RaycastHit raycastHit2;
        Debug.DrawRay(transform.position, playerPos, Color.green); //For debugging to confirm the ray is working

        if (Physics.Raycast(transform.position, playerPos, out raycastHit, aggroDistance))
        {
            // Look at the player
            RotateTowards(player.transform);
            //Check if player is in FOV (set in UNITY) and that the returned racyastHit is the player and not a wall
            if (angle < enemyFOV && raycastHit.collider.gameObject.tag == "Player")
            {
                //Start following the player
                following = true;
                SelectPath();

                if (Vector3.Distance(transform.position, player.transform.position) <= spellAttackDistance
                    && (playerScript.notBeingAttacked || playerScript.notBeingAttacked2) && !attacking)
                {
                    if(Vector3.Distance(transform.position, player.transform.position) <= meleeAttackDistance)
                    {
                        StartCoroutine(AttackMelee());
                    }
                    else
                    {
                        int randomAttack = Random.Range(0, 10);
                        if (randomAttack > 7)
                        {
                            StartCoroutine(AttackMighty());
                        }
                        else
                        {
                            StartCoroutine(Attack());
                        }
                    }
                }
            }
            else if (following)
            {
                SelectPath();
            }
        }
        // Check if Player is within a larger "aggroLossDistance" and if the player has been initially aggroed before it starts following
        else if (Physics.Raycast(transform.position, playerPos, out raycastHit2, aggroLossDistance) && following)
        {
            NavMeshPath path = new NavMeshPath();
            navMeshAgent.CalculatePath(player.transform.position, path);
            navMeshAgent.SetPath(path);
        }
        // If player is farther than aggroLoss, the enemy will no longer follow and walk the waypoints
        else
        {
            following = false;
            WalkWaypoints();
        }
    }

    void WalkWaypoints()
    {
        //Transform cachedPosition = transform;
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance) // < stoppingdistance
        {
            Vector3 newPosition = RandomNavSphere(transform.position, wanderRadius, -1);
            navMeshAgent.SetDestination(newPosition);
        }
    }

    IEnumerator Attack()
    {
        attacking = true;
        if (playerScript.notBeingAttacked == false)
        {
            playerScript.notBeingAttacked2 = false;
        }
        else
        {
            playerScript.notBeingAttacked = false;
        }
        animator.SetTrigger("attacking");
        ShootProjectile();
        yield return new WaitForSeconds(attackPause);
        attacking = false;
    }

    IEnumerator AttackMelee()
    {
        attacking = true;
        if (playerScript.notBeingAttacked == false)
        {
            playerScript.notBeingAttacked2 = false;
        }
        else
        {
            playerScript.notBeingAttacked = false;
        }
        animator.SetTrigger("attackingMelee");
        yield return new WaitForSeconds(attackPause);
        attacking = false;
    }
    IEnumerator AttackMighty()
    {
        attacking = true;
        if (playerScript.notBeingAttacked == false)
        {
            playerScript.notBeingAttacked2 = false;
        }
        else
        {
            playerScript.notBeingAttacked = false;
        }
        animator.SetTrigger("attackingMighty");
        ShootMightyProjectile();
        yield return new WaitForSeconds(attackPause);
        attacking = false;
    }
    void SelectPath()
    {
        NavMeshPath path = new NavMeshPath();

        int randomDir = Random.Range(0, 10);
        if (randomDir < 3) //left
        {
            Vector3 offset = transform.position - player.transform.position;
            moveDirection = Vector3.Cross(offset, Vector3.up);
            navMeshAgent.CalculatePath(player.transform.position + moveDirection, path);
            navMeshAgent.SetPath(path);
        }
        else if (randomDir >= 3 && randomDir < 5) //right
        {
            Vector3 offset = player.transform.position - transform.position;
            moveDirection = Vector3.Cross(offset, Vector3.up);
            navMeshAgent.CalculatePath(player.transform.position + moveDirection, path);
            navMeshAgent.SetPath(path);
        }
        else
        {
            //to keep away
            //playPos = player.transform.position = player.transform.position + player.transform.forward * 10   ??
            navMeshAgent.CalculatePath(player.transform.position, path);
            navMeshAgent.SetPath(path);
        }
    }

    void Death()
    {
        if (health <= 0 && !isDead)
        {
            LootDrop();
            isDead = true;
            animator.SetBool("isDead", true);
            navMeshAgent.SetDestination(transform.position);
            body.velocity = Vector3.zero;
            if (body.velocity == Vector3.zero)
            {
                bodyCollider.isTrigger = true;
            }
            Destroy(this.gameObject, 5f);
        }
    }

    void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10f);
    }

    void LootDrop()
    {
        if (GetComponent<Loot>().hasKey == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(keyDrop, newPos, transform.rotation);
        }
        if (GetComponent<Loot>().hasFire == true && GetComponent<Loot>().hasKey == false)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(fireDrop, newPos, transform.rotation);
        }
        else if (GetComponent<Loot>().hasFire == true && GetComponent<Loot>().hasKey == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            newPos.x += 1;
            Instantiate(fireDrop, newPos, transform.rotation);
        }
    }
    public void ShootProjectile()
    {
        tempObj = Instantiate(projectile, transform.position + Vector3.up, transform.rotation) as GameObject;
       // tempObj.transform.position += Vector3.forward;

        Rigidbody rb = tempObj.GetComponent<Rigidbody>();
        rb.velocity = transform.forward * 7;
        Destroy(tempObj, 3f);
    }
    public void ShootMightyProjectile()
    {
        tempObj = Instantiate(mightyProjectile, transform.position + Vector3.up, transform.rotation) as GameObject;
     //   tempObj.transform.position += Vector3.forward;

        Rigidbody rb = tempObj.GetComponent<Rigidbody>();
        rb.velocity = transform.forward * 10;
        Destroy(tempObj, 3f);
    }
}
