﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flicker : MonoBehaviour
{
    public float MinimumIntensity = 1f;
    public float MaximumIntensity = 3f;
    public float Speed = 1f;

    private Light lightComponent;
    private float random;
    void Start()
    {
        lightComponent = GetComponent<Light>();
        random = Random.Range(0, 5000);
    }
    
    void Update()
    {
        //This creates a random value for the intensity of the flame
        float noise = Mathf.PerlinNoise(random, Time.time * Speed);
        lightComponent.intensity = Mathf.Lerp(MinimumIntensity, MaximumIntensity, noise);
    }
}
