﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ButtonOverride : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData pointerEventData)
    {
        if (pointerEventData.button == PointerEventData.InputButton.Left)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false; //Take out for Web
            //Debug.Log("Clicked");
        }
        else if (pointerEventData.button == PointerEventData.InputButton.Right)
        {
            return;
        }
    }

    //public void ClickLock()
    //{
    //    Cursor.lockState = CursorLockMode.Locked;
    //}

}
