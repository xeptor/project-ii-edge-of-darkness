﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    public Animator animator;
    private int levelToLoad;
    private float volume;
    public static LevelLoader instance = null;

    public GameObject loadingScreen;
    public Slider slider;
    GameObject sound;
    public GameObject rawImage;

    public void Start()
    {
        //volume = audioManager.instance.music.volume;
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        volume = sound.GetComponent<audioManager>().music.volume;
    }

    public void FadeToLevel (int levelIndex)
    {
        levelToLoad = levelIndex;
        animator.SetTrigger("FadeOut");

        //while (audioManager.instance.music.volume > 0)
        //    audioManager.instance.music.volume -= volume * Time.deltaTime / 1000;
        //audioManager.instance.music.Stop();
        StartCoroutine(LoadAsynchrounously(levelToLoad));
        //audioManager.instance.music.Play();
        //while (audioManager.instance.music.volume < 0.227)
        //    audioManager.instance.music.volume += volume * Time.deltaTime / 1000;
    }

    IEnumerator LoadAsynchrounously(int levelIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(levelToLoad);

        loadingScreen.SetActive(true);
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);

            slider.value = progress;

            yield return null;
        }
    }
    
    void SetActive()
    {
        if (gameObject != null)
            gameObject.SetActive(true);
    }

    void PlayVideo()
    {
        if (rawImage != null)
            rawImage.SetActive(true);
    }

    public void Respawn()
    {
        if (PlayerPrefs.GetInt("Hardcore") == 1)
        {
            FadeToLevel(1);
        }
        else
        {
            FadeToLevel(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
