﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableObject : MonoBehaviour
{
    public enum DropType
    {
        GuaranteedDrop,
        RandomItemDrop,
        NoDrop
    }
    
    public uint randomItemDropRange = 30;
    public DropType typeOfDrop;
    public GameObject GuaranteedDrop;
    public GameObject SecondaryDrop;
    public float xDifBreak = 1.5f;
    public float yDifBreak = 1.5f;
    public float zDifBreak = 1.5f;

    private float timer = 0;
    private float durationAlive;
    private bool animationTriggered = false;
    private int onDestroyID;
    private Vector3 originalPosition;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerSword")
        {
            Break();
        }
    }
    void Start()
    {
        //durationAlive = GetComponentInChildren<ParticleSystem>().main.startLifetime.constant;
        durationAlive = GetComponentInChildren<AudioSource>().clip.length;
        GetComponentInChildren<ParticleSystem>().Stop();

        //generate a random ID for item drop
        onDestroyID = Random.Range(0, (int)randomItemDropRange + 1);

        //Save the original position for later
        originalPosition = GetComponent<Transform>().position;
    }
    void Update()
    {
        if (!animationTriggered)
            if (Mathf.Abs(originalPosition.x - GetComponent<Transform>().position.x) > xDifBreak || Mathf.Abs(originalPosition.y - GetComponent<Transform>().position.y) > yDifBreak || Mathf.Abs(originalPosition.z - GetComponent<Transform>().position.z) > zDifBreak)
                Break();
        if (animationTriggered)
        {
            timer += Time.deltaTime;
            if (timer >= durationAlive)
                Destroy(gameObject, 0);
        }
    }
    private void DropItem()
    {
        Vector3 newPos = transform.position;
        newPos.y = 1;

        switch (typeOfDrop)
        {
            case DropType.GuaranteedDrop:
                //drops the first item every time
                if (GuaranteedDrop != null)
                    Instantiate(GuaranteedDrop, newPos, transform.rotation);
                break;
            case DropType.RandomItemDrop:
                //determine what the breakable does when it explodes
                switch (onDestroyID)
                {
                    case 1:
                        if (GuaranteedDrop != null)
                            Instantiate(GuaranteedDrop, newPos, transform.rotation);
                        break;
                    case 2:
                        if (SecondaryDrop != null)
                            Instantiate(SecondaryDrop, newPos, transform.rotation);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
    private void Break()
    {
        GetComponentInChildren<AudioSource>().Play();
        DropItem();
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<MeshCollider>().enabled = false;
        GetComponentInChildren<ParticleSystem>().Play();
        animationTriggered = true;
    }
}
