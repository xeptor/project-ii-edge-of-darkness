﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialAttackDamage : MonoBehaviour
{
    float damageAmount;
    float knockback;
    bool poisonous = false;
    int poisonTimer = 0;
    float poisonDamage = 0;
    void Start()
    {
        damageAmount = GetComponentInParent<EnemyStats>().specialDamage;
        knockback = GetComponentInParent<EnemyStats>().specialKnockback;
        poisonous = GetComponentInParent<EnemyStats>().specialPoisonous;
        poisonTimer = GetComponentInParent<EnemyStats>().specialPoisonTimer;
        poisonDamage = GetComponentInParent<EnemyStats>().specialPoisonDamage;
    }
    void Update()
    {

    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            collider.GetComponent<PlayerController>().TakeDamage(damageAmount, (collider.transform.position - GetComponent<Transform>().root.position), knockback, poisonous, poisonTimer, poisonDamage);
        }
    }
}
