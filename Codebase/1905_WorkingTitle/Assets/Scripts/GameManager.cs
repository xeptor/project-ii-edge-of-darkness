﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public BossController boss;
    public GameObject winScreen;
    public LevelLoader levelLoader;

    void Update()
    {
        if (boss != null && boss.isDead)
        {
            StartCoroutine(WinGame());
        }
    }

    public void LoadScene(int sceneIndex)
    {
        levelLoader.FadeToLevel(sceneIndex);
    }

    public void PlayGame()
    {
        levelLoader.FadeToLevel(1);
    }

    IEnumerator WinGame()
    {
        yield return new WaitForSeconds(5f);
        winScreen.SetActive(true);
    }

    public void QuitGame()
    {
        Debug.Log("Exit");
        Application.Quit();
    }
}
