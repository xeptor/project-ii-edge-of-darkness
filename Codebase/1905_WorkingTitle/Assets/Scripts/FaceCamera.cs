﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceCamera : MonoBehaviour
{
    GameObject player;

    private void Start()
    {
        player = GameObject.Find("Player");
    }
    void LateUpdate()
    {
        transform.localEulerAngles = new Vector3(0, player.transform.localEulerAngles.y - 180, 0);

    }
}
