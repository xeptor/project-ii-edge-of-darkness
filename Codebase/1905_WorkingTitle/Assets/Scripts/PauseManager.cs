﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    public GameObject[] pauseObjects;
    GameObject[] gameOverObjects;
    GameObject[] optionsObjects;
    GameObject reticle;
    public GameObject hud;
    void Start()
    {
        Time.timeScale = 1;
        pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
        gameOverObjects = GameObject.FindGameObjectsWithTag("ShowOnEnd");
        optionsObjects = GameObject.FindGameObjectsWithTag("ShowOnOptions");
        reticle = GameObject.FindGameObjectWithTag("Reticle");
        hud = GameObject.Find("HUD");
        hidePaused();
        hideEnded();
        hideOptionsStart();
        showReticle();
    }
    
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape) && !optionsObjects[0].activeSelf && !pauseObjects[0].activeSelf)
        {
            if (Time.timeScale == 1)
            {

                Time.timeScale = 0;
                Cursor.visible = true;  //Take out for web
                Cursor.lockState = CursorLockMode.None;
                showPaused();
                hideReticle();
            }
            else if (Time.timeScale == 0)
            {
                Time.timeScale = 1;
                Cursor.visible = false; //Take out for web
                Cursor.lockState = CursorLockMode.Locked;
                hidePaused();
                hideOptionsStart();
                showReticle();
            }
        }

        if (hud.GetComponent<PlayerElements>().currentHealth <= 0)
        {
            Time.timeScale = 0;
            Cursor.visible = true;  //Take out for web
            Cursor.lockState = CursorLockMode.None;
            showEnded();
            hideReticle();
        }

    }
    
    public void pauseControl()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            showPaused();
            hideReticle();
        }
        else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            hidePaused();
            showReticle();
        }
    }


    public void MainMenu()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.None;
            hidePaused();
            hideOptionsStart();
            hideEnded();
        }
    }
    
    public void showPaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
    }
    
    public void hidePaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
    }

    public void hideOptionsStart()
    {
        foreach (GameObject g in optionsObjects)
        {
            g.SetActive(false);
        }
    }
    public void hideOptions()
    {
        foreach (GameObject g in optionsObjects)
        {
            Time.timeScale = 1;
            g.SetActive(false);
        }
    }

    public void showOptions()
    {
        foreach (GameObject g in optionsObjects)
        {
            Time.timeScale = 0;
            g.SetActive(true);
        }
    }

    public void showEnded()
    {
        foreach (GameObject g in gameOverObjects)
        {

            g.SetActive(true);
        }
    }

    //hides objects with ShowOnFinish tag
    public void hideEnded()
    {
        foreach (GameObject g in gameOverObjects)
        {
            g.SetActive(false);
        }
    }

    public void hideReticle()
    {
        reticle.SetActive(false);
    }
    public void showReticle()
    {
        reticle.SetActive(true);
    }

    public void LoadScene(int level)
    {
        SceneManager.LoadScene(level);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
    
}
