﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class DoorOpen : MonoBehaviour
{
    public bool unlocked = false;
    public AudioClip doorOpenSFX;
    public int currScene;
    public bool goesToNextLevel;
    public bool isLocked;

    private int playerScene;
    private ParticleSystem[] particles;
    private GameObject playerCharacter;
    private GameObject loader;
    private LevelLoader levelLoader;
    GameObject sound;

    void Start()
    {
        particles = GetComponentsInChildren<ParticleSystem>();
        loader = GameObject.Find("LevelLoader");
        levelLoader = loader.GetComponent<LevelLoader>();
        playerCharacter = GameObject.Find("Player");
        sound = GameObject.FindGameObjectWithTag("SoundManager");
    }

    void Update()
    {
    }

    void ChangeLevel()
    {
        if (goesToNextLevel)
        {
            //transition scene
            //Change level to load
            playerCharacter.GetComponent<PlayerController>().currentScene++;
            currScene++;
            //save health and stamina
            playerCharacter.GetComponent<PlayerController>().SavePlayerInfo();
            //call the level loader here
            levelLoader.FadeToLevel(currScene);
        }
    }

    void OnTriggerEnter(Collider player)
    {
        //if player enters collider
        if (player.gameObject.tag == "Player")
        {
            if ((player.GetComponentInParent<PlayerController>().hasKey || unlocked) && !isLocked)
            {
                //Play SFX
                //audioManager.instance.PlaySound(doorOpenSFX);
                sound.GetComponent<audioManager>().PlaySound(doorOpenSFX);
                //turn off door animation
                particles[0].Stop();
                particles[1].Stop();

                if (!unlocked)
                {
                    //if you used a key remove it
                    player.GetComponentInParent<PlayerController>().hasKey = false;

                    //Store an unlock for later so you don't need a key
                    unlocked = true;
                }

                //disable wall
                particles[0].GetComponent<BoxCollider>().enabled = false;

                ChangeLevel();
            }
        }
    }

    private void OnTriggerExit(Collider player)
    {
        if (unlocked)
        {
            //enable wall
            particles[0].GetComponent<BoxCollider>().enabled = true;
            //turn on door animation
            particles[0].Play();
            particles[1].Play();

            //Play SFX
            //audioManager.instance.PlaySound(doorOpenSFX);
        }
    }
}
