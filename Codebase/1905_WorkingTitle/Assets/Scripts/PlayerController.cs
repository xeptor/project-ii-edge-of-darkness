﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    
    CharacterController characterController;
    ParticleSystem particleEffect;
    public Projectile fireBall;
    PlayerElements plElements;
    public GameObject sword;
    public GameObject player;
    public GameObject hud;
    public GameObject gameOverScreen;
    public Animator playerAnim;
    public Animator swordAnim;
    public ParticleSystem dashParticles;

    //hud elements
    


    int comboNum = 0;
    ////////poison variables////////
    float poisonTimer = 0;
    int poisonCurrentTime;
    public float poisonDamage;
    ////////////////////////////////
    public float cooldownTime;
    public float maxCooldown = 1.25f;
    public bool isPoisoned;
    public bool notBeingAttacked = true;
    public bool notBeingAttacked2 = true;
    public bool hasKey = false;
    public bool canAttack = true;
    
    public float attackPause = .25f;
    public int speed = 6;
    public float health;  //Player health set in plElements
    int speedOrig;
    public int dashSpeed = 50;
    public float dashTime = 0.3f;
    public int attackType;
    public float stamina = 100;
    public float damageToEnemy; 
    public float attackDamage = 20;      // 
    public float powerAttackDamage = 75; // ^ Change these two in inspector for different damage amounts
    public float gravity = 20.0f;

    private float damageFromEnemy;
    public float magnitude;
    public float staminaCost = 25;
    public float powerAttackCost = 50;
    
    public float MaxCombatTime;
    float CombatTimer;
    
    private bool step = true;
    private Vector3 moveDirection = Vector3.zero;
    public Vector3 pushDirection = Vector3.zero;

    public int currentScene = 1;

    //Audio
    public AudioClip grunt1;
    public AudioClip grunt2;
    public AudioClip grunt3;
    public AudioClip leftFoot;
    public AudioClip rightFoot;
    public AudioClip dashSFX;
    GameObject sound;

    //power attack cooldown
    public float maxPowerCooldown = 100;
    public float currPowerCooldown;
    public float cooldownPowerRegen = .5f;

    

    private void Awake()
    {
        
        if (PlayerPrefs.GetFloat("Health") == 0)
        {
            PlayerPrefs.SetFloat("Health", 100);
            PlayerPrefs.SetFloat("Stamina", 100);
            PlayerPrefs.SetInt("Orbs", 0);
            PlayerPrefs.SetInt("FirePots", 0);
            PlayerPrefs.SetFloat("PowerCooldown", 100);
        }
    }
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        swordAnim = sword.GetComponent<Animator>();
        speedOrig = speed;
        fireBall = GetComponent<Projectile>();
        particleEffect = GetComponentInChildren<ParticleSystem>();
        player = GameObject.Find("Player");
        hud = GameObject.Find("HUD");
        plElements = hud.GetComponent<PlayerElements>();
        sound = GameObject.FindGameObjectWithTag("SoundManager");
        dashParticles.Stop();
        
        LoadPlayerInfo();

        isPoisoned = false;
        CombatTimer = MaxCombatTime;
        
    }

    void FixedUpdate()
    {
        health = plElements.currentHealth;
        //Take poison damage
        if (isPoisoned == true)
            TakePoisonDamage();
        if (currPowerCooldown < maxPowerCooldown)
            currPowerCooldown += cooldownPowerRegen;
        
        
    }
    void Update()
    {
        EnemyCombatManager();

        pushDirection.x = Mathf.Lerp(pushDirection.x, 0, Time.deltaTime * 4);
        pushDirection.y = Mathf.Lerp(pushDirection.y, 0, Time.deltaTime * 50);
        pushDirection.z = Mathf.Lerp(pushDirection.z, 0, Time.deltaTime * 4);
        if (Input.GetKey("left shift") && Input.GetButtonDown("Attack"))
        {
            
            if (currPowerCooldown == 100 && plElements.currentStam >= (powerAttackCost) && canAttack)
            {
                StartCoroutine(powerAttack(5));
            }
            
        }
        if (canAttack)
        {
            
            if (Input.GetButtonDown("Attack") && !Input.GetKey("left shift"))
            {
                cooldownTime = maxCooldown;
                if (comboNum == 0)
                {
                    StartCoroutine(attack(2));
                }
                if (comboNum == 1)
                {
                    StartCoroutine(attack(3));
                }
                if (comboNum == 2)
                {
                    StartCoroutine(attack(4));
                }
                if (comboNum != 3)
                {
                    comboNum += 1;
                }
                else
                {
                    comboNum = 0;
                }

            }
            
            if (Input.GetKeyDown(KeyCode.X))
            {
                if (plElements.currentFirePotion > 0)
                {
                    plElements.UseFlask();
                    fireBall.ShootProjectile();
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (health == 100 && plElements.currentStam == 100)
                return;
            else
                plElements.UsePotion();

        }
        if (Input.GetButtonDown("Dash") && plElements.currentStam >= staminaCost && characterController.velocity.magnitude > 0)
        {
            StartCoroutine(dash());
        }
        if (cooldownTime > 0)
        {
            cooldownTime -= Time.deltaTime;

        }
        else
        {
            comboNum = 0;
        }
        if (health <= 0)
        {
            Death();
            // Debug.Log("Dead");
        }
        Movement();
        if (transform.position.y <= -10)
        {
            ResetPlayerPosition();
        }
    }

    void EnemyCombatManager()
    {
        // If the player is being attacked, count down the timer for the enemy to attack
        if (!notBeingAttacked && !notBeingAttacked2)
        {
            // Counts down timer
            CombatTimer -= Time.deltaTime;

            // If the timer is less than or equal to zero set the player to not being attacked
            if (CombatTimer <= MaxCombatTime/2)
            {
                notBeingAttacked = true;
            }
            if (CombatTimer <= 0)
            {
                notBeingAttacked2 = true;
                CombatTimer = MaxCombatTime;
            }
        }
    }

    void Movement()
    {
        if (characterController.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
        }
        if (characterController.isGrounded && characterController.velocity.magnitude > 3 && step)
        {
            Walking();
        }
        characterController.Move((moveDirection + pushDirection * magnitude) * speed * Time.deltaTime);

        moveDirection.y -= gravity * Time.deltaTime;
    }
    
    public void TakeDamage(float damage, Vector3 knockback, float magnitudeOfKnockback, bool poisonous, int poisonTime, float damageFromPoison)
    {
        //Poison segment
        if (poisonous)
        {
            poisonDamage += damageFromPoison;
            poisonCurrentTime += poisonTime;
            isPoisoned = true;
            gameObject.transform.Find("PoisonScreen").gameObject.SetActive(true);
        }
        //set magnitude of knockback
        magnitude = magnitudeOfKnockback;

        //magnitude = knockback;
        pushDirection = knockback;

        //Play Blood Splatter
        StartCoroutine(plElements.Damage());

        //Update the HUD and Health
        health -= damage;
        plElements.currentHealth = health;

        //Play grunt effect feedback
        sound.GetComponent<audioManager>().RandomSound(grunt1, grunt2, grunt3);
    }

    void TakePoisonDamage()
    {
        if (isPoisoned == true)
        {
            if (poisonCurrentTime > 0)
            {
                poisonTimer += Time.deltaTime;
                if (poisonTimer >= 1)
                {
                    health -= (poisonDamage / poisonCurrentTime);
                    plElements.currentHealth = health;
                    poisonDamage -= poisonDamage / poisonCurrentTime;
                    poisonTimer--;
                    poisonCurrentTime--;
                }
            }
            if (poisonCurrentTime <= 0)
            {
                isPoisoned = false;
                gameObject.transform.Find("PoisonScreen").gameObject.SetActive(false);
            }
        }
    }

    public void Cure()
    {
        poisonCurrentTime = 0;
        poisonDamage = 0;
        poisonTimer = 0;
    }

    void Death()
    {

        // Add delay and sound effect
        // we want to have a game over screen pop up and only call respawn if the player selects that option
        if (gameOverScreen != null)
            gameOverScreen.SetActive(true);
    }

    void ResetPlayerPosition()
    {
        //place player at beginning
        transform.position = GameObject.FindGameObjectWithTag("PlayerSpawn").transform.position;
    }

    public void SavePlayerInfo()
    {
        PlayerPrefs.SetFloat("Health", plElements.currentHealth);
        PlayerPrefs.SetFloat("Stamina", plElements.currentStam);
        
        PlayerPrefs.SetInt("Orbs", plElements.currentOrbsOfLight);
        PlayerPrefs.SetInt("FirePots", plElements.currentFirePotion);
        PlayerPrefs.SetFloat("PowerCooldown", currPowerCooldown);
    }

    public void LoadPlayerInfo()
    {
        plElements.currentHealth = PlayerPrefs.GetFloat("Health");
        plElements.currentStam = PlayerPrefs.GetFloat("Stamina");
        plElements.currentOrbsOfLight = PlayerPrefs.GetInt("Orbs");
        plElements.currentFirePotion = PlayerPrefs.GetInt("FirePots");
        currPowerCooldown = PlayerPrefs.GetFloat("PowerCooldown");
        PlayerPrefs.DeleteKey("Health");
        PlayerPrefs.DeleteKey("Stamina");
        PlayerPrefs.DeleteKey("Cooldown");
        PlayerPrefs.DeleteKey("Orbs");
        PlayerPrefs.DeleteKey("FirePots");
    }

    IEnumerator dash()
    {
        speed = dashSpeed;
        dashParticles.Play();
        sound.GetComponent<audioManager>().PlayWalking(dashSFX);
        plElements.currentStam -= staminaCost;
        yield return new WaitForSeconds(dashTime);
        dashParticles.Stop();
        speed = speedOrig;
       
    }

    IEnumerator attack(int attackType)
    {
        canAttack = false;
        damageToEnemy = attackDamage;
        swordAnim.SetTrigger("attackTrigger");
        swordAnim.SetInteger("condition", attackType);
        yield return new WaitForSeconds(attackPause);
        canAttack = true;
    }

    IEnumerator powerAttack(int attackType)
    {
        
        canAttack = false;
        
        damageToEnemy = powerAttackDamage;        
        plElements.currentStam -= powerAttackCost;
        currPowerCooldown = 0;
        swordAnim.SetTrigger("attackTrigger");
        swordAnim.SetInteger("condition", attackType);
        yield return new WaitForSeconds(attackPause);
        

        canAttack = true;
    }

    IEnumerator screenGreen()
    {
        hud.transform.Find("PoisonScreen").gameObject.SetActive(true);
        yield return new WaitForSeconds(.2f);
        hud.transform.Find("PoisonScreen").gameObject.SetActive(false);
    }

    void Walking()
    {
        StartCoroutine(FootSteps());
    }

    IEnumerator FootSteps()
    {
        step = false;
        //audioManager.instance.PlayWalking(leftFoot);
        sound.GetComponent<audioManager>().PlayWalking(leftFoot);
        yield return new WaitForSeconds(0.4f);
        //audioManager.instance.PlayWalking(rightFoot);
        sound.GetComponent<audioManager>().PlayWalking(rightFoot);
        yield return new WaitForSeconds(0.45f);
        step = true;
    }
}
