﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GoblinCCM : MonoBehaviour
{
    public bool isDead = false;
    // Start is called before the first frame update
    public Collider swordCollider;
    public Collider bodyCollider;
    public Collider fangsCollider;
    public GameObject keyDrop;
    public GameObject fireDrop;
    public GameObject healthDrop;
    public GameObject player;
    public GameObject hud;
    public NavMeshAgent navMeshAgent;
    PlayerElements playerElements;
    ParticleSystem particleEffect;
    Animator animator;
    Rigidbody body;
    Loot loot;
    PlayerController playerScript;

    public bool hasFirePot = false;
    public bool hasHealthPot = false;
    public bool hasAnyPot = false;
    bool following = false;
    public bool canAttack = true;
    public float attackPause = 3.0f;
    public float attackDistance = 3.5f;
    public float health = 100;
    public int enemyFOV = 180;
    public int aggroDistance = 7;
    public int aggroLossDistance = 30;
    float damageFromPlayer;
    float attackDecision = 0;

    // *Teacher said to take out bufferDistance
    //Vector3 bufferDistance = new Vector3(2f/*.7f*/, 0, 2f/*0*/);

    //Enenmy direction
    Vector3 moveDirection;

    //Wander
    public float wanderRadius = 10.0f;

    //Path
    public float maxPathDecisionTime = 0.5f;
    private float pathTimer = 0.0f;

    void Start()
    {
        //Find the player object
        player = GameObject.Find("Player");
        bodyCollider = GetComponent<Collider>();
        swordCollider = GameObject.Find("Sword").GetComponentInChildren<Collider>();
        body = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        particleEffect = GetComponentInChildren<ParticleSystem>();
        playerScript = player.GetComponent<PlayerController>();
        hud = GameObject.Find("HUD");
        playerElements = hud.GetComponent<PlayerElements>();
        loot = GetComponent<Loot>();

        //Enemy begins walking
        WalkWaypoints();

        //Initialize NavMesh path finding timer
        pathTimer = 0.0f;
    }

    void Update()
    {
        //Increase path decision timer
        pathTimer += Time.deltaTime;
        // Get the speed of the NavMeshAgent
        float speed = navMeshAgent.velocity.magnitude;
        animator.SetFloat("speed", speed);

        //Check if health is 0 then execute death sequence
        Death();

        if (!isDead && health > 0)
        {
            if (!hasAnyPot)
            {
                RayCastFollowOrAttack();
            }
            else
            {
                RunAndHide();
            }
        }
    }

    //Random pos
    public static Vector3 RandomNavSphere(Vector3 origin, float distance, int layer)
    {
        Vector3 randomDirection = Random.insideUnitSphere * distance;

        randomDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randomDirection, out navHit, distance, layer);

        return navHit.position;
    }

    public static Vector3 RunFromPlayer(Vector3 origin, float distance, int layer)
    {
        Vector3 awayFromPlayer = new Vector3(1, 0, 1) * distance;

        awayFromPlayer += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(awayFromPlayer, out navHit, distance, layer);

        return navHit.position;
    }

    void OnTriggerEnter(Collider other)
    {
        //Check for collison with the Players sword
        if (other.gameObject.tag == "PlayerSword")
        {
            //Update Health
            if (health > 0 && !isDead)
            {
                StartCoroutine(Damage());
            }
        }
    }

    IEnumerator Damage()
    {
        damageFromPlayer = playerScript.damageToEnemy;
        animator.SetTrigger("damage");
        health -= damageFromPlayer;
        if (damageFromPlayer > 199 && health > 0)
        {
            navMeshAgent.enabled = false;
            body.isKinematic = false;
            Vector3 direction = (player.transform.position - transform.position).normalized;
            body.AddForce(direction.x * -750, 0, direction.z * -750);
            yield return new WaitForSeconds(.4f);
            navMeshAgent.enabled = true;
            body.isKinematic = true;
        }
        else if (damageFromPlayer > 50 && health > 0)
        {
            navMeshAgent.enabled = false;
            body.isKinematic = false;
            Vector3 direction = (player.transform.position - transform.position).normalized;
            body.AddForce(direction.x * -500, 0, direction.z * -500);
            yield return new WaitForSeconds(.4f);
            navMeshAgent.enabled = true;
            body.isKinematic = true;
        }
    }

    private void RayCastFollowOrAttack()
    {
        //FIND THE PLAYER AND LOCK ON WHEN "SEEN"
        //Distance
        Vector3 playerPos = player.transform.position - transform.position;
        //Angle between player position and forward vector
        float angle = Vector3.Angle(playerPos, transform.forward);

        RaycastHit raycastHit;
        RaycastHit raycastHit2;
        Debug.DrawRay(transform.position, playerPos, Color.green); //For debugging to confirm the ray is working

        if (Physics.Raycast(transform.position, playerPos, out raycastHit, aggroDistance))
        {
            //Check if player is in FOV (set in UNITY) and that the returned racyastHit is the player and not a wall
            if (angle < enemyFOV && raycastHit.transform == player.transform)
            {
                //Start following the player
                following = true;
                if (pathTimer > maxPathDecisionTime)
                {
                    pathTimer -= maxPathDecisionTime;
                    SelectPath();
                }
                // Look at the player
                RotateTowards(player.transform);

                if (Vector3.Distance(transform.position, player.transform.position) < attackDistance
                    && (playerScript.notBeingAttacked || playerScript.notBeingAttacked2))
                {
                    StartCoroutine(Attack());
                }
            }
        }
        // Check if Player is within a larger "aggroLossDistance" and if the player has been initially aggroed before it starts following
        else if (Physics.Raycast(transform.position, playerPos, out raycastHit2, aggroLossDistance) && following)
        {
            if (pathTimer > maxPathDecisionTime)
            {
                pathTimer -= maxPathDecisionTime;
                SelectPath();
            }
        }
        // If player is farther than aggroLoss, the enemy will no longer follow and walk the waypoints
        else
        {
            following = false;
            WalkWaypoints();
        }
    }

    void WalkWaypoints()
    {
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance) // < stoppingdistance
        {
            Vector3 newPosition = RandomNavSphere(transform.position, wanderRadius, -1);
            navMeshAgent.SetDestination(newPosition);
        }
    }
    void RunAndHide()
    {
        //NavMeshPath path = new NavMeshPath();

        //navMeshAgent.CalculatePath(player.transform.forward, path);
        //navMeshAgent.SetPath(path);

        Vector3 newPosition = RunFromPlayer(player.transform.position, 20, -1);
        navMeshAgent.SetDestination(newPosition);

        if (navMeshAgent.remainingDistance < 3)
        {
            RayCastFollowOrAttack();

            newPosition = RunFromPlayer(player.transform.position, 20, -1);
            navMeshAgent.SetDestination(newPosition);
            //navMeshAgent.CalculatePath(player.transform.forward, path);
            //navMeshAgent.SetPath(path);
        }

        Death();
    }

    void RandomizeAttack()
    {
        attackDecision = Random.Range(0f, 1f);
    }

    IEnumerator Attack()
    {
        // Tell Combat manager another enemy can be attacked
        //playerScript.notBeingAttacked = false;
        if (playerScript.notBeingAttacked == false)
        {
            playerScript.notBeingAttacked2 = false;
        }
        else
        {
            playerScript.notBeingAttacked = false;
        }

        RandomizeAttack();

        //animator.SetFloat("attackNumber", 1);
        //animator.SetBool("isAttacking", true);
        if (playerElements.currentOrbsOfLight >= 1 && !hasAnyPot)
        {
            animator.SetBool("isAttacking", false);
            animator.SetBool("isSpecialAttacking", true);
        }
        else if (playerElements.currentFirePotion >= 1 && !hasAnyPot)
        {
            animator.SetBool("isAttacking", false);
            animator.SetBool("isSpecialAttacking", true);
        }
        else
        {
            animator.SetBool("isSpecialAttacking", false);
            animator.SetBool("isAttacking", true);
        }
        yield return new WaitForSeconds(attackPause);
        animator.SetFloat("attackNumber", 0);
        animator.SetBool("isAttacking", false);
        animator.SetBool("isSpecialAttacking", false);
    }

    void SelectPath()  //New Pathfinding function
    {
        NavMeshPath path = new NavMeshPath();

        int randomDir = Random.Range(0, 10);
        if (randomDir < 3) //left
        {
            Vector3 offset = transform.position - player.transform.position;
            moveDirection = Vector3.Cross(offset, Vector3.up);
            navMeshAgent.CalculatePath(player.transform.position + moveDirection, path);
            navMeshAgent.SetPath(path);
        }
        else if (randomDir >= 3 && randomDir < 5) //right
        {
            Vector3 offset = player.transform.position - transform.position;
            moveDirection = Vector3.Cross(offset, Vector3.up);
            navMeshAgent.CalculatePath(player.transform.position + moveDirection, path);
            navMeshAgent.SetPath(path);
        }
        else
        {
            navMeshAgent.CalculatePath(player.transform.position, path);
            navMeshAgent.SetPath(path);
        }
    }

    void Death()
    {
        if (health <= 0 && !isDead)
        {
            DecideLoot();
            LootDrop();
            isDead = true;
            animator.SetBool("isDead", true);
            navMeshAgent.SetDestination(transform.position);
            body.velocity = Vector3.zero;
            if (body.velocity == Vector3.zero)
            {
                //able to walk through and pick up key immediately on death
                swordCollider.isTrigger = true;
                bodyCollider.isTrigger = true;
            }
            Destroy(this.gameObject, 4f);
        }
    }

    //Teacher said doesnt need to be co-routine
    void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10f);
    }

    void StealHealthPotion()
    {
        playerElements.currentOrbsOfLight -= 1;
        hasHealthPot = true;
        hasAnyPot = true;
    }
    void StealFirePotion()
    {
        playerElements.currentFirePotion -= 1;
        hasFirePot = true;
        hasAnyPot = true;
    }
    public void HmmWhatToSteal()
    {
        if (playerElements.currentOrbsOfLight >= 1 && !hasAnyPot)
        {
            StealHealthPotion();
        }
        else if (playerElements.currentFirePotion >= 1 && !hasAnyPot)
        {
            StealFirePotion();
        }
    }

    void DecideLoot()
    {
        if (hasHealthPot == true)
        {
            loot.hasHealth = true;
        }
        else if (hasFirePot == true)
        {
            loot.hasFire = true;
        }
    }
    void LootDrop()
    {
        if (GetComponent<Loot>().hasKey == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(keyDrop, newPos, transform.rotation);
        }
        if (GetComponent<Loot>().hasHealth == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(healthDrop, newPos, transform.rotation);
        }
        if (GetComponent<Loot>().hasFire == true && GetComponent<Loot>().hasKey == false)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(fireDrop, newPos, transform.rotation);
        }
        else if (GetComponent<Loot>().hasFire == true && GetComponent<Loot>().hasKey == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            newPos.x += 1;
            Instantiate(fireDrop, newPos, transform.rotation);
        }
    }
}
