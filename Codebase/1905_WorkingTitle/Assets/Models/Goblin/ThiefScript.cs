﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThiefScript : MonoBehaviour
{
    public Animator animator;
    public GoblinCCM goblin;
    public RectTransform indicatorRect;
    public Image indicatorImage;
    float angle = 0;
    Color targetAlpha;

    float damageAmount;
    float knockback;
    bool poisonous = false;
    int poisonTimer = 0;
    float poisonDamage = 0;

    private int hit = 0;
    void Start()
    {
        damageAmount = GetComponentInParent<EnemyStats>().damage;
        knockback = GetComponentInParent<EnemyStats>().knockback;
        poisonous = GetComponentInParent<EnemyStats>().poisonous;
        poisonTimer = GetComponentInParent<EnemyStats>().poisonTimer;
        poisonDamage = GetComponentInParent<EnemyStats>().poisonDamage;

        indicatorImage = GameObject.FindGameObjectWithTag("indicatorImage").transform.Find("indicator").GetComponent<Image>();
        indicatorRect = indicatorImage.GetComponent<RectTransform>();
        indicatorImage.CrossFadeAlpha(0, 0, false);
    }
    void Update()
    {

    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            if (hit == 0)
            {
                //Debug.Log("Hit");
                hit++;
                StealItem();
                Debug.Log(damageAmount);
                collider.GetComponent<PlayerController>().TakeDamage(damageAmount, (collider.transform.position - GetComponent<Transform>().root.position), knockback, poisonous, poisonTimer, poisonDamage);
                Debug.Log(damageAmount);
                // Angle between other pos vs player
                angle = GetHitAngle(collider.transform, transform.parent.transform.forward);
                indicatorImage.CrossFadeAlpha(1, 0, false);
                indicatorRect.rotation = Quaternion.Euler(0, 0, -angle);
                indicatorImage.CrossFadeAlpha(0, 1, false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (hit == 1)
        {
            hit = 0;
        }
    }
    public float GetHitAngle(Transform player, Vector3 incomingDirection)
    {
        // Flatten to plane
        var otherDir = new Vector3(-incomingDirection.x, 0f, -incomingDirection.z);
        var playerFwd = Vector3.ProjectOnPlane(player.forward, Vector3.up);

        // Direction between player fwd and incoming object
        var angle = Vector3.SignedAngle(playerFwd, otherDir, Vector3.up);

        return angle;
    }
    void StealItem()
    {
        if (animator.GetBool("isSpecialAttacking"))
        {
            goblin.HmmWhatToSteal();
        }
    }
}
