﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpiderCCM : MonoBehaviour
{
    public bool isDead = false;
    // Start is called before the first frame update
    public Collider swordCollider;
    public Collider bodyCollider;
    public Collider fangsCollider;
    public GameObject keyDrop;
    public GameObject fireDrop;
    public GameObject player;
    public GameObject hud;
    public NavMeshAgent navMeshAgent;
    ParticleSystem particleEffect;
    public Animator animator;
    Rigidbody body;
    PlayerController playerScript;
    public AudioClip squash;

    bool following = false;
    public bool canAttack = true;
    public float attackPause = 3.0f;
    public float attackDistance = 3.5f;
    public float health = 100;
    public int enemyFOV = 180;
    public int aggroDistance = 7;
    public int aggroLossDistance = 30;
    float damageFromPlayer;
    float attackDecision = 0;
    //bool deathPoisoned = false;
    int hitCounter = 0;

    //Enenmy direction
    Vector3 moveDirection;

    //Wander
    public float wanderRadius = 10.0f;

    //Path
    public float maxPathDecisionTime = 0.5f;
    private float pathTimer = 0.0f;

    //Sound
    GameObject sound;
    public AudioClip spiderHit;

    void Start()
    {
        //Find the player object
        player = GameObject.Find("Player");
        bodyCollider = GetComponent<Collider>();
        swordCollider = GameObject.Find("Sword").GetComponentInChildren<Collider>();
        fangsCollider = GameObject.Find("Fangs").GetComponentInChildren<Collider>();
        body = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        particleEffect = GetComponentInChildren<ParticleSystem>();
        playerScript = player.GetComponent<PlayerController>();
        hud = GameObject.Find("HUD");
        sound = GameObject.FindGameObjectWithTag("SoundManager");

        //Enemy begins walking
        WalkWaypoints();

        //Initialize NavMesh path finding timer
        pathTimer = 0.0f;
    }

    void Update()
    {
        //Increase path decision timer
        pathTimer += Time.deltaTime;
        // Get the speed of the NavMeshAgent
        float speed = navMeshAgent.velocity.magnitude;
        animator.SetFloat("speed", speed);

        //Check if health is 0 then execute death sequence
        Death();
        if (!isDead)
        {
            RayCastFollowOrAttack();
        }

    }

    //Random pos
    public static Vector3 RandomNavSphere(Vector3 origin, float distance, int layer)
    {
        Vector3 randomDirection = Random.insideUnitSphere * distance;

        randomDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randomDirection, out navHit, distance, layer);

        return navHit.position;
    }

    void OnTriggerEnter(Collider other)
    {
        //Check for collison with the Players sword
        if (other.gameObject.tag == "PlayerSword")
        {
            //Update Health
            if (health > 0 && !isDead)
            {
                StartCoroutine(Damage());
            }
        }
        //// Cameron - Will poision the player if stepped on after death
        //if (!deathPoisoned && isDead && other.gameObject.tag == "Player")
        //{
        //    deathPoisoned = true;
        //    player.GetComponent<PlayerController>().isPoisoned = true;
        //    //audioManager.instance.PlayEnemySound(squash);
        //    sound.GetComponent<audioManager>().PlayEnemySound(squash);
        //}
    }

    IEnumerator Damage()
    {
        damageFromPlayer = playerScript.damageToEnemy;
        if (hitCounter == 3)
        {
            animator.SetTrigger("damage");
            hitCounter = 0;
        }
        else
        {
            particleEffect.Play();
            sound.GetComponent<audioManager>().PlayEnemySound(spiderHit);
            hitCounter++;
        }
        health -= damageFromPlayer;
        if (damageFromPlayer > 199 && health > 0)
        {
            navMeshAgent.enabled = false;
            body.isKinematic = false;
            Vector3 direction = (player.transform.position - transform.position).normalized;
            body.AddForce(direction.x * -750, 0, direction.z * -750);
            yield return new WaitForSeconds(.4f);
            navMeshAgent.enabled = true;
            body.isKinematic = true;
        }
        else if (damageFromPlayer > 50 && health > 0)
        {
            navMeshAgent.enabled = false;
            body.isKinematic = false;
            Vector3 direction = (player.transform.position - transform.position).normalized;
            body.AddForce(direction.x * -500, 0, direction.z * -500);
            yield return new WaitForSeconds(.4f);
            navMeshAgent.enabled = true;
            body.isKinematic = true;
        }
    }

    private void RayCastFollowOrAttack()
    {
        //FIND THE PLAYER AND LOCK ON WHEN "SEEN"
        //Distance
        Vector3 playerPos = player.transform.position - transform.position;
        //Angle between player position and forward vector
        float angle = Vector3.Angle(playerPos, transform.forward);

        RaycastHit raycastHit;
        RaycastHit raycastHit2;
        Debug.DrawRay(transform.position, playerPos, Color.green); //For debugging to confirm the ray is working

        if (Physics.Raycast(transform.position, playerPos, out raycastHit, aggroDistance))
        {
            // Look at the player
            RotateTowards(player.transform);
            //Check if player is in FOV (set in UNITY) and that the returned racyastHit is the player and not a wall
            if (angle < enemyFOV && raycastHit.transform == player.transform)
            {
                //Start following the player
                following = true;
                if (pathTimer > maxPathDecisionTime)
                {
                    pathTimer -= maxPathDecisionTime;
                    SelectPath();
                }


                if (Vector3.Distance(transform.position, player.transform.position) < attackDistance
                    && (playerScript.notBeingAttacked || playerScript.notBeingAttacked2))
                {
                    StartCoroutine(Attack());
                }
            }
            else if (following)
            {
                SelectPath();
            }
        }
        // Check if Player is within a larger "aggroLossDistance" and if the player has been initially aggroed before it starts following
        else if (Physics.Raycast(transform.position, playerPos, out raycastHit2, aggroLossDistance) && following)
        {
            NavMeshPath path = new NavMeshPath();
            navMeshAgent.CalculatePath(player.transform.position, path);
            navMeshAgent.SetPath(path);
        }
        // If player is farther than aggroLoss, the enemy will no longer follow and walk the waypoints
        else
        {
            following = false;
            WalkWaypoints();
        }
    }

    void WalkWaypoints()
    {
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance) // < stoppingdistance
        {
            Vector3 newPosition = RandomNavSphere(transform.position, wanderRadius, -1);
            navMeshAgent.SetDestination(newPosition);
        }
    }

    void RandomizeAttack()
    {
        attackDecision = Random.Range(0f, 1f);
    }

    IEnumerator Attack()
    {
        // Tell Combat manager another enemy can be attacked
        //playerScript.notBeingAttacked = false;
        if (playerScript.notBeingAttacked == false)
        {
            playerScript.notBeingAttacked2 = false;
        }
        else
        {
            playerScript.notBeingAttacked = false;
        }

        RandomizeAttack();

        //animator.SetFloat("attackNumber", 1);
        //animator.SetBool("isAttacking", true);
        if (attackDecision > .75f)
        {
            animator.SetBool("isAttacking", true);
            animator.SetBool("isSpecialAttacking", false);
        }
        else
        {
            animator.SetBool("isSpecialAttacking", true);
            animator.SetBool("isAttacking", false);
        }
        yield return new WaitForSeconds(attackPause);
        //if (player.transform.Find("PoisonScreen"))
        //{
        //    player.transform.Find("PoisonScreen").gameObject.SetActive(false);
        //}
        animator.SetFloat("attackNumber", 0);
        animator.SetBool("isAttacking", false);
        animator.SetBool("isSpecialAttacking", false);
    }

    void SelectPath()  //New Pathfinding function
    {
        NavMeshPath path = new NavMeshPath();

        int randomDir = Random.Range(0, 10);
        if (randomDir < 3) //left
        {
            Vector3 offset = transform.position - player.transform.position;
            moveDirection = Vector3.Cross(offset, Vector3.up);
            navMeshAgent.CalculatePath(player.transform.position + moveDirection, path);
            navMeshAgent.SetPath(path);
        }
        else if (randomDir >= 3 && randomDir < 5) //right
        {
            Vector3 offset = player.transform.position - transform.position;
            moveDirection = Vector3.Cross(offset, Vector3.up);
            navMeshAgent.CalculatePath(player.transform.position + moveDirection, path);
            navMeshAgent.SetPath(path);
        }
        else
        {
            navMeshAgent.CalculatePath(player.transform.position, path);
            navMeshAgent.SetPath(path);
        }
    }
    IEnumerator Poison()
    {
        if (animator.GetBool("isSpecialAttacking"))
        {
            if (swordCollider.gameObject.tag == "Player")
            {
                hud.transform.Find("PoisonScreen").gameObject.SetActive(true);
            }
            yield return new WaitForSeconds(3f);
            hud.transform.Find("PoisonScreen").gameObject.SetActive(false);
        }
    }

    void Death()
    {
        if (health <= 0 && !isDead)
        {
            LootDrop();
            isDead = true;
            animator.SetBool("isDead", true);
            navMeshAgent.SetDestination(transform.position);
            body.velocity = Vector3.zero;
            if (body.velocity == Vector3.zero)
            {
                //able to walk through and pick up key immediately on death
                swordCollider.isTrigger = true;
                bodyCollider.isTrigger = true;
            }
            Destroy(this.gameObject, 4f);
        }
    }

    //Teacher said doesnt need to be co-routine
    void RotateTowards(Transform target)
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 10f);
    }
    void LootDrop()
    {
        if (GetComponent<Loot>().hasKey == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(keyDrop, newPos, transform.rotation);
        }
        if (GetComponent<Loot>().hasFire == true && GetComponent<Loot>().hasKey == false)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            Instantiate(fireDrop, newPos, transform.rotation);
        }
        else if (GetComponent<Loot>().hasFire == true && GetComponent<Loot>().hasKey == true)
        {
            Vector3 newPos = transform.position;
            newPos.y += 1;
            newPos.x += 1;
            Instantiate(fireDrop, newPos, transform.rotation);
        }
    }
}
